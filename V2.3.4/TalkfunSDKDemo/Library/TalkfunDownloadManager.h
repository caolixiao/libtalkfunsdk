//
//  TalkfunDownloadManager.h
//  TalkfunSDKDemo
//
//  Created by 孙兆能 on 16/7/22.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TalkfunSDK.h"

//下载状态(未开始  、 暂停  、 等待中 、 正在下载  、 下载完成 、 下载错误)
typedef enum {
    
    TalkfunDownloadStatusUnstart    = 0, //未开始
    TalkfunDownloadStatusPrepare    = 1, //等待中
    TalkfunDownloadStatusStart      = 2, //正在下载
    TalkfunDownloadStatusPause      = 3, //暂停
    TalkfunDownloadStatusFinish     = 4, //下载完成
    TalkfunDownloadStatusError      = 5  //下载错误
    
}TalkfunDownloadStatus;

/**
 *  缓存路径
 */
typedef enum {
    
    TalkfunCachePathDocuments = 0,
    TalkfunCachePathLibrary   = 1
    
}TalkfunCachePath;

@interface TalkfunDownloadManager : NSObject

//下载线程数(最大为3）
@property (nonatomic,assign) NSInteger downloadThreadNumber;
@property (nonatomic,assign) TalkfunCachePath cachePath;

/**
 *  单例
 */
+ (nullable id)shareManager;

/**
 *  下载某个点播
 *
 *  @param access_token是点播的access_token（不可为空）
 *  @param placbackID是点播对应的ID（不可为空）
 *  @param title是显示在下载列表中的title（可传nil,显示playbackID）
 */
- (void)appendDownloadWithAccessToken:(nonnull NSString *)access_token playbackID:(nonnull NSString *)playbackID title:(nullable NSString *)title;

/**
 *  下载某个点播
 *
 *  @param accessKey不可为空
 *  @param placbackID是点播对应的ID（不可为空）
 *  @param title是显示在下载列表中的title（可传nil,显示playbackID）
 */
- (void)appendDownloadWithAccessKey:(nonnull NSString *)accessKey playbackID:(nonnull NSString *)playbackID title:(nullable NSString *)title;

/**
 *  获取下载列表
 */
- (nullable NSArray *)getDownloadList;

/**
 *  全部下载任务暂停
 */
- (void)pauseAllDownload;

/**
 *  开始对应的下载任务
 *
 *  playbackID是对应的任务
 */
- (void)startDownload:(nonnull NSString *)playbackID;

/**
 *  暂停对应的下载任务
 *
 *  playbackID是对应的任务
 */
- (void)pauseDownload:(nonnull NSString *)playbackID;

/**
 *  删除对应的点播
 *
 *  删除是否成功的信息在callback中
 */
- (void)deleteDownload:(nonnull NSString *)playbackID success:(void (^ __nullable)(id __nullable result))successBlock;

/**
 *  监听事件，处理回调数据
 *
 *  监听事件，event为事件名，获取的数据在回调的callback里面
 **/
- (void)on:(nullable NSString *)event withCallback:(void (^__nullable)(id __nullable result))callback;

/**
 *  判断下载列表是否有对应playbackID
 **/
- (BOOL)containsPlaybackID:(nonnull NSString *)playbackID;

/**
 *  按照playbackID获取对应信息
 **/
- (nullable id)getInfoWithPlaybackID:(nonnull NSString *)playbackID;

@end
