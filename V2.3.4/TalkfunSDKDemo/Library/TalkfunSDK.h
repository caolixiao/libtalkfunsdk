//
//  TalkfunSDKViewController.h
//  TalkfunSDK
//
//  Created by 孙兆能 on 16/5/27.
//  Copyright © 2016年 talk－fun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TalkfunDownloadManager.h"

extern NSString * const TalkfunSDKVersion;

/**
 *  是否为点播的参数配置名，初始化SDK时的参数
 */
extern NSString * const TalkfunIsPlayback;

/**
 *  用户事件（public）
 */
extern NSString * const TALKFUN_EVENT_PLAY;                 //播放
extern NSString * const TALKFUN_EVENT_CAMERA_SHOW;          //摄像头开
extern NSString * const TALKFUN_EVENT_CAMERA_HIDE;          //摄像头关
extern NSString * const TALKFUN_EVENT_LIVE_DESKTOP_START;   //桌面分享开始
extern NSString * const TALKFUN_EVENT_LIVE_DESKTOP_STOP;    //桌面分享结束
extern NSString * const TALKFUN_EVENT_LIVE_DESKTOP_PAUSE;   //桌面分享暂停
extern NSString * const TALKFUN_EVENT_LIVE_MODE_CHANGE;     //模式切换
/**
 *  用户事件（live）
 */
extern NSString * const TALKFUN_EVENT_NETWORK_SPEED;        //网络速度
extern NSString * const TALKFUN_EVENT_VOTE_NEW;             //投票开始
extern NSString * const TALKFUN_EVENT_VOTE_PUB;             //投票结束
extern NSString * const TALKFUN_EVENT_LOTTERY_START;        //抽奖开始
extern NSString * const TALKFUN_EVENT_LOTTERY_STOP;         //抽奖结束
extern NSString * const TALKFUN_EVENT_BROADCAST;            //广播
extern NSString * const TALKFUN_EVENT_CHAT_DISABLE;         //禁言
extern NSString * const TALKFUN_EVENT_CHAT_DISABLE_ALL;     //全体禁言
extern NSString * const TALKFUN_EVENT_CHAT_SEND;            //信息发送接收
extern NSString * const TALKFUN_EVENT_FLOWER_SEND;          //送花
extern NSString * const TALKFUN_EVENT_FLOWER_GET_INIT;      //送花初始化
extern NSString * const TALKFUN_EVENT_FLOWER_TOTAL;         //花朵总数
extern NSString * const TALKFUN_EVENT_FLOWER_TIME_LEFT;     //送花剩余时间
extern NSString * const TALKFUN_EVENT_QUESTION_LIST;        //问题列表
extern NSString * const TALKFUN_EVENT_QUESTION_ASK;         //提问
extern NSString * const TALKFUN_EVENT_QUESTION_REPLY;       //问题回复
extern NSString * const TALKFUN_EVENT_QUESTION_DELETE;      //问题删除
extern NSString * const TALKFUN_EVENT_MEMBER_JOIN_ME;       //我进入房间
extern NSString * const TALKFUN_EVENT_MEMBER_JOIN_OTHER;    //其他人进入房间
extern NSString * const TALKFUN_EVENT_MEMBER_LEAVE;         //其他人离开房间
extern NSString * const TALKFUN_EVENT_MEMBER_TOTAL;         //房间总人数
extern NSString * const TALKFUN_EVENT_MEMBER_KICK;          //踢人
extern NSString * const TALKFUN_EVENT_MEMBER_FORCEOUT;      //被强迫退出房间
extern NSString * const TALKFUN_EVENT_ANNOUNCE_NOTICE;      //公告
extern NSString * const TALKFUN_EVENT_ANNOUNCE_ROLL;        //滚动公告
extern NSString * const TALKFUN_EVENT_LIVE_START;           //直播开始
extern NSString * const TALKFUN_EVENT_LIVE_STOP;            //直播结束
extern NSString * const TALKFUN_EVENT_ROOM_INIT;            //房间初始化
/**
 *  用户事件（playback）
 */
extern NSString * const TALKFUN_EVENT_VOD_START;            //点播开始
extern NSString * const TALKFUN_EVENT_VOD_STOP;             //点播结束
extern NSString * const TALKFUN_EVENT_LIVE_DURATION;        //点播总时长
extern NSString * const TALKFUN_EVENT_LIVE_INFO;            //点播信息
extern NSString * const TALKFUN_EVENT_LIVE_QUESTION_APPEND; //问题列表
extern NSString * const TALKFUN_EVENT_LIVE_MESSAGE_APPEND;  //聊天信息列表
extern NSString * const TALKFUN_EVENT_LIVE_CHAPTER_LIST;    //章节列表

/**
 *  广播名
 */
extern NSString * const TalkfunErrorNotification;       //错误信息广播（类型看错误信息类型）

/**
 用户角色
 **/
extern NSString * const TalkfunMemberRoleSpadmin;       //超级管理员(老师)
extern NSString * const TalkfunMemberRoleAdmin;         //管理员(助教)
extern NSString * const TalkfunMemberRoleUser;          //普通用户(学生)
extern NSString * const TalkfunMemberRoleGuest;         //游客

extern int const TalkfunMemberGenderUnknow;             //未知性别
extern int const TalkfunMemberGenderMale;               //男性
extern int const TalkfunMemberGenderFemale;             //女性

extern NSString * const TalkfunPlaybackID;              //点播离线播放的key

//鉴权类型
typedef NS_ENUM(NSInteger,TalkfunAuthenticationMode){
    TalkfunAuthenticationByAccessToken = 20000, //通过accessToken鉴权
    TalkfunAuthenticationByAccessKey = 20001    //通过accessKey鉴权
};

//错误信息类型
typedef enum {
    
    TalkfunErrorLoadingData = 10000,             //加载数据出错(html、js等)
    TalkfunErrorNetworkDisconnect = 10001,       //网络连接错误
    TalkfunErrorVideoError = 10002,              //视频加载错误
    TalkfunErrorDownloadError = 10003,           //下载错误
    TalkfunErrorLoadingOfflineFileError = 10004  //加载离线文件错误
    
}TalkfunError;

//状态码
typedef enum{
    
    TalkfunCodeSuccess = 0,
    TalkfunCodeFailed  = -1,
    TalkfunCodeTimeout = -100,
    
}TalkfunCode;

//播放状态
typedef enum {
    
    TalkfunPlayStatusStop    = 0,
    TalkfunPlayStatusPlaying = 1,
    TalkfunPlayStatusPause   = 2,
    TalkfunPlayStatusSeeking = 3,
    TalkfunPlayStatusError   = 4
    
}TalkfunPlayStatus;

//视频模式
typedef enum {
    
    TalkfunLiveModeVideo   = 0,
    TalkfunLiveModeDesktop = 2
    
}TalkfunLiveMode;

//网络状态
typedef enum {

    TalkfunNetworkStatusGood    = 0,
    TalkfunNetworkStatusGeneral = 1,
    TalkfunNetworkStatusBad     = 2
    
}TalkfunNetworkStatus;

//SDK代理
@protocol TalkfunSDKDelegate <NSObject>

@optional
/**
 *  网络状态发生变化时候调用
 **/
- (void)talkfunPlayStatusChange:(TalkfunPlayStatus)status;

@end

@interface TalkfunSDK : UIViewController<UIScrollViewDelegate>

//代理
@property (nonatomic,weak) id<TalkfunSDKDelegate> delegate;
//获取当前token
@property (nonatomic,copy,readonly)     NSString * access_token;
//是否是点播（默认是直播）
@property (nonatomic,readonly)         BOOL isPlayback;
//获取当前播放视频的模式
@property (nonatomic,assign,readonly)   TalkfunLiveMode liveMode;
//获取视频窗口容器
@property (nonatomic,weak,readonly)   UIView * cameraContainerView;
//获取ppt容器
@property (nonatomic,weak,readonly)   UIView * pptContainerView;
//获取播放桌面分享的容器
@property (nonatomic,weak,readonly)   UIView * desktopContainerView;
//获取视频的播放状态
@property (nonatomic,assign,readonly) TalkfunPlayStatus playStatus;
//点播播放速度 [0-2]
@property (nonatomic) float playbackRate;

/**
 start to enter live
 **/

/**
 *  TalkfunSDK单例
 **/
+ (TalkfunSDK *)shareInstance;

/**
 *  初始化TalkfunSDK，需要传入AccessToken和parameters（传入是否点播等参数）
 **/
- (TalkfunSDK *)initWithAccessToken:(NSString *)token parameters:(NSMutableDictionary *)parameters;

/**
 *  初始化TalkfunSDK，需要传入AccessKey和parameters（传入是否点播等参数）
 **/
- (TalkfunSDK *)initWithAccessKey:(NSString *)accessKey parameters:(NSMutableDictionary *)parameters;

/**
 *  传入appID和appSecret来初始化TalkfunSDK，parameters参数同上
 **/
- (TalkfunSDK *)initWithAppID:(NSString *)appID appSecret:(NSString *)appSecret parameters:(NSMutableDictionary *)parameters;

/**
 *  配置appID和appSecret
 **/
- (void)configureAppID:(NSString *)appID appSecret:(NSString *)appSecret;

/**
 *  设置log
 *
 *  可以控制SDK里面打印的东西
 **/
- (void)setLog:(BOOL)log;

/**
 *  配置摄像头容器（可用于重新设置摄像头容器）
 *
 *  初始化完SDK后即可调用
 **/
- (void)configureCameraContainerView:(UIView *)cameraContainerView;

/**
 *  配置ppt容器（可用于重新设置ppt容器）
 *
 *  初始化完SDK后即可调用
 **/
- (void)configurePPTContainerView:(UIView *)pptContainerView;

/**
 *  配置桌面分享容器(默认是ppt容器)
 *
 *  初始化完SDK后即可调用
 **/
- (void)configureDesktopContainerView:(UIView *)desktopContainerView;

/**
 *  向服务器发送消息，处理回调数据
 *
 *  监听事件，event为事件名，获取的数据在回调的callback里面
 **/
- (void)emit:(NSString *)event parameter:(NSDictionary *)parameter callback:(void (^)(id obj))callback;
/**
 *  向服务器发送消息，处理回调数据
 *
 *  监听事件，event为事件名，获取的数据在回调的callback里面
 **/
- (void)emit:(NSString *)event withParameter:(NSDictionary *)parameter withCallback:(void (^)(id obj))callback NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, "该方法已过期，请查看TalkfunSDK.h头文件");

/**
 *  监听事件，处理回调数据
 *
 *  监听事件，event为事件名，获取的数据在回调的callback里面
 **/
- (void)on:(NSString *)event callback:(void (^)(id obj))callback;
/**
 *  监听事件，处理回调数据
 *
 *  监听事件，event为事件名，获取的数据在回调的callback里面
 **/
- (void)on:(NSString *)event withCallback:(void (^)(id obj))callback  NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, "该方法已过期，请查看TalkfunSDK.h头文件");

/**
 *  获取网络列表(直播)
 *
 **/
- (void)getNetworkList:(void (^)(id result))callback;

/**
 *  设置网络(直播)
 *
 *  把相应网络的key作为operatorID参数传进SDK，key在获取网络列表中得到
 **/
- (void)setNetwork:(NSString *)operatorID sourceName:(NSString *)sourceName selectedSegmentIndex:(NSInteger)selectedSegmentIndex;

/**
 *  传入线路path
 *
 **/
-(void)lineSwitching:(NSString*)path;

/**
 *  获取网络列表(点播)
 *
 **/
- (NSArray *)getNetworkLinesList;

/**
 *  设置播放对应的线路(点播)
 *
 *  把相应线路传进SDK
 **/
- (void)setNetworkLine:(NSNumber *)networkLineIndex;

/**
 *  设置播放进度（点播）
 *
 *  点播的时候想seek到某个时间就调用这个方法把时间传进SDK，单位是秒
 **/
- (void)setPlayDuration:(CGFloat)duration;

/**
 *  暂停
 *
 *  点播或者直播都可以暂停，一般只是用于点播的暂停
 **/
- (void)pause;

/**
 *  进入后台时是否暂停(默认暂停YES)
 **/
- (void)setPauseInBackground:(BOOL)pause;

/**
 *  播放
 *  在暂停之后的播放调用
 **/
- (void)play;

/**
 *  隐藏摄像头（切换音视频源，只有音频）
 *
 *  隐藏摄像头的时候切换到只有音频的模式，不调用这个方法的话只是隐藏了摄像头而没有真正切换
 **/
- (void)hideCamera;

/**
 *  显示摄像头（有音视频）
 *
 *  显示摄像头的时候切换到有音视频的模式
 **/
- (void)showCamera;

/**
 *  交换ppt容器和摄像头容器，在你切换ppt和摄像头容器的时候调用
 **/
- (void)exchangePPTAndCameraContainer;

/**
 *  切换token
 *
 *  当你需要切换accessToken又不销毁SDK时调用，playbackID是离线播放时候用的playbackID，不需要可以传nil
 **/
- (void)configureAccessToken:(NSString * )token playbackID:(NSString *)playbackID;

/**
 *  设置白名单
 *
 *  SDK为了拦截不必要的东西接管了网络请求，如果在打开了SDK之后需要进行其它的网络请求，需要调用这个方法把其它请求的url或url的域名传进SDK
 **/
- (void)setDomainWhitelist:(NSArray *)list;

/**
 *  销毁SDK
 *
 *  销毁SDK时调用以便彻底销毁SDK
 **/
- (void)destroy;

@end
