//
//  AppDelegate.m
//  TalkfunSDKDemo
//
//  Created by talk－fun on 15/11/24.
//  Copyright © 2015年 talk-fun. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewViewController.h"
#import "NavLoginViewController.h"
//#import <Bugtags/Bugtags.h>
#import "TalkfunViewController.h"
#import <Bugly/Bugly.h>
#import "NetworkDetector.h"
#import "TalkfunDownloadManager.h"
#include "AFNetworking.h"
#import "TalkfunPlaybackViewController.h"
@interface AppDelegate ()<UIAlertViewDelegate>

@property (nonatomic,strong) NetworkDetector * networkDetector;
@property (nonatomic,strong) UIAlertView * networkAlertView;
@property (nonatomic,strong) TalkfunDownloadManager * downloadManager;
@property(nonatomic,strong)NSString*releaseNotes;

@property(nonatomic,strong)NSString*trackViewUrl;

@end

@implementation AppDelegate

- (TalkfunDownloadManager *)downloadManager
{
    if (!_downloadManager) {
        _downloadManager = [TalkfunDownloadManager shareManager];
    }
    return _downloadManager;
}

- (UIAlertView *)networkAlertView
{
    if (!_networkAlertView) {
        _networkAlertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定不在wifi环境下观看或下载?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    }
    return _networkAlertView;
}

- (NetworkDetector *)networkDetector
{
    if (!_networkDetector) {
        __weak typeof(self) weakSelf = self;
        _networkDetector = [[NetworkDetector alloc] init];
        _networkDetector.networkChangeBlock = ^(NetworkStatus networkStatus){
            
            if (networkStatus != 1) {
                [weakSelf.networkAlertView show];
            }
            else
            {
                [weakSelf.networkAlertView dismissWithClickedButtonIndex:0 animated:YES];
            }
        };
    }
    return _networkDetector;
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [NSThread sleepForTimeInterval:2.0];
    [self.networkDetector networkcheck];
    
    [Bugly startWithAppId:@"900042949"];
    
    LoginViewViewController * loginVC = [[LoginViewViewController alloc] init];
    NavLoginViewController * nav = [[NavLoginViewController alloc] initWithRootViewController:loginVC];
    self.window.rootViewController = nav;
    
    //         ios8后，需要添加这个注册，才能得到授权
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType type =  UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:type categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        //             通知重复提示的单位，可以是天、周、月
        //                        notification.repeatInterval = kCFCalendarUnitSecond;
#pragma mark 设置NSCalendarUnitDay即代表每天重复
        //            cation.repeatInterval = NSCalendarUnitDay;
    }
    
#pragma mark - 版本更新检测
    [self checkForUpdate];
    
    
    return YES;
}
//josn转字典
- (NSDictionary *)DictionaryConversionString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

// 当通过URL把我的应用程序打开时,会执行该方法
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
    // 1.将url转成字符串
    NSString *urlString = url.absoluteString;
    
    NSString * str = [urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSUInteger origin = [str rangeOfString:@"TalkfunSDK://"].location + 13;
    NSString *End = [str substringFromIndex:origin];
    
    NSDictionary *getDict =   [self DictionaryConversionString:End];
    
    //点播
    if ([getDict[@"type"]isEqualToString:@"vod"]) {
        [self vodWithDict:getDict];
        //直播
    }else  if ([getDict[@"type"] isEqualToString:@"live"]){
        [self liveWithDict:getDict];
    }
    
    return YES;
}

-(NSString*) removeLastOneChar:(NSString*)origin
{
    NSString* cutted;
    if([origin length] > 0){
        cutted = [origin substringToIndex:([origin length]-2)];// 去掉最后一个","
    }else{
        cutted = origin;
    }
    return cutted;
}

- (void)vodWithDict:(NSDictionary *)getDict
{
    NSString *access_token =    getDict[@"access_token"];
    
    if(access_token){
        
        
        
        NSString *liveid =  getDict[@"liveid"];
        
        //2.拿到主页控制器
        NavLoginViewController  *rootViewController = (NavLoginViewController *)self.window.rootViewController;
        LoginViewViewController *rootVc = [rootViewController.childViewControllers firstObject];
        
        //   跳转到点播 界面
        TalkfunPlaybackViewController *playbackVC = [[TalkfunPlaybackViewController alloc ]init];
        //      TalkfunView.access_token = access_token;
        //    TalkfunView.playbackID = liveid;
        //
        
        
        //TODO:原生模式的点播
        //    TalkfunPlaybackViewController * playbackVC = [TalkfunPlaybackViewController new];
        playbackVC.playbackID = liveid;
        playbackVC.res = @{@"data":@{@"access_token":access_token},TalkfunPlaybackID:liveid};
        
        //    [self presentViewController:playbackVC animated:NO completion:nil];
        
        [rootVc presentViewController:playbackVC animated:NO completion:nil];
    }
}

- (void)liveWithDict:(NSDictionary *)dict
{
    //2.拿到主页控制器
    NavLoginViewController  *rootViewController = (NavLoginViewController *)self.window.rootViewController;
    LoginViewViewController *rootVc = [rootViewController.childViewControllers firstObject];
    
    //   跳转到直播界面
    TalkfunViewController *TalkfunView = [[TalkfunViewController alloc ]init];
    
    //    TalkfunView.token = dict[@"access_token"];
    //    TalkfunView.autoStart = YES;
    //
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:dict[@"access_token"] forKey:@"access_token"];
    
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject:data forKey:@"data"];
    [result setObject:@(0) forKey:@"code"];
    
    TalkfunView.res = result;
    
    [rootVc presentViewController:TalkfunView animated:NO completion:nil];
    
}
+ (void)registerLocalNotification:(NSString *)alertTime AndBody:(NSString *)alerbody{
    UILocalNotification *notification = ({
        UILocalNotification *cation =    [[UILocalNotification alloc] init];
        //        NSDateFormatter *dataFormater = ({
        //            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        //            [formatter setDateFormat:@"HH:mm"];
        //            formatter;
        //        });
        
        //        NSDate *fireDate = [dataFormater dateFromString:alertTime];
        NSDate * currentDate = [NSDate date];
        //        通知时间
        cation.fireDate = currentDate;
        //         通知内容
        cation.alertBody =  alerbody;
        
        //        cation.alertAction = @"dakaidongdong";
        
        NSInteger badgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber;
        
        NSLog(@"%ld",(long)badgeNumber);
        
        cation.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
        //通知被触发时播放的声音
        cation.soundName = UILocalNotificationDefaultSoundName;
        //通知参数
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:alerbody forKey:@"key"];
        cation.userInfo = userDict;
        //ios8后，需要添加这个注册，才能得到授权
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            UIUserNotificationType type =  UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:type categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        }
        cation;
    });
    //    执行通知注册
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    //    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    self.backgroundSessionCompletionHandle = completionHandler;
    //添加本地通知
    //    [self presentNotification];
}

//- (void)presentNotification{
//    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//    localNotification.alertBody = @"下载完成!";
//    localNotification.alertAction = @"后台传输下载已完成!";
//    //提示音
//    localNotification.soundName = UILocalNotificationDefaultSoundName;
//    //icon提示加1
//    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
//    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
//}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    //    NSLog(@"Application did receive local notifications");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:notification.alertBody delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"BigBoss送信" message:notification.alertBody preferredStyle:UIAlertControllerStyleAlert];
    [alert show];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}


#pragma mark - 版本更新检测
- (void)checkForUpdate {
    // 本地版本
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSString *identifier = [[NSBundle mainBundle] bundleIdentifier];
    
    //商店版本
    NSString *url = [NSString stringWithFormat:@"https://itunes.apple.com/cn/lookup?bundleId=%@",identifier];
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    
    [mgr GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *responseDict = responseObject;
        if ([responseDict[@"resultCount"] integerValue] == 0) {
            return;
        }
        // 商店版本
        NSString *storeVersion = [(NSArray *)responseDict[@"results"] firstObject][@"version"];
        // 更新内容
        self.releaseNotes = [(NSArray *)responseDict[@"results"] firstObject][@"releaseNotes"];
        // 商店链接
        self.trackViewUrl = [(NSArray *)responseDict[@"results"] firstObject][@"trackViewUrl"];
        
        //        NSLog(@"\n\nappVersion = %@\nstoreVersion = %@\ntrackViewUrl = %@ @\trackViewUrl = %@ ",appVersion, storeVersion, self.trackViewUrl,self.releaseNotes);
        
        [self compareString:appVersion string:storeVersion];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
    
}

- (void)compareString:(NSString *)appVersion string:(NSString *)storeVersion {
    NSArray *currentVersionStr = [self makeArray:[appVersion componentsSeparatedByString:@"."]];
    NSArray *newVersionStr = [self makeArray:[storeVersion componentsSeparatedByString:@"."]];
    if ([newVersionStr[0] integerValue] > [currentVersionStr[0] integerValue]) {
        [self alertUpdateVC:storeVersion];
    }else if ([newVersionStr[0] integerValue] == [currentVersionStr[0] integerValue]) {
        if ([newVersionStr[1] integerValue] > [currentVersionStr[1] integerValue]) {
            [self alertUpdateVC:storeVersion];
        }else if ([newVersionStr[1] integerValue] == [currentVersionStr[1] integerValue]) {
            if ([newVersionStr[2] integerValue] > [currentVersionStr[2] integerValue]) {
                [self alertUpdateVC:storeVersion];
            }
        }
    }
}
- (void)alertUpdateVC:(NSString *)storeVersion {
    NSString *title = [NSString stringWithFormat:@"检测到新版本:%@",storeVersion];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:self.releaseNotes preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        UIAlertAction *updateAction = [UIAlertAction actionWithTitle:@"查看" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.trackViewUrl]];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:updateAction];
        
        
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:self.releaseNotes delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"查看", nil];
        [alertView show];
        alertView.tag = 122;
        
    }
}
- (NSArray *)makeArray:(NSArray *)array {
    NSMutableArray *arr = [NSMutableArray arrayWithArray:array];
    // 两位数字的版本号前面加0
    for (NSInteger i = 0; i < 3 - array.count; i ++) {
        [arr addObject:@"0"];
    }
    
    return arr;
}

#pragma mark - alertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.networkAlertView) {
        if (buttonIndex == 0) {
            [self.downloadManager pauseAllDownload];
        }
    }else if (alertView.tag == 122) {
        if (buttonIndex == 1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.trackViewUrl]];
        }
    }
}

@end
