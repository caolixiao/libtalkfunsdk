//
//  TalkfunPlaybackViewController.h
//  TalkfunSDKDemo
//
//  Created by 孙兆能 on 2016/11/28.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TalkfunPlaybackViewController : UIViewController

@property (nonatomic,strong) NSDictionary * res;
//@property (nonatomic,assign) BOOL isProtrait;
@property (nonatomic,copy) NSString * playbackID;
@property (nonatomic,copy) NSString *access_token;
@end
