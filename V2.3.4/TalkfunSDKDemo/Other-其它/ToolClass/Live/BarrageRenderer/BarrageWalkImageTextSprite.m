//
//  BarrageWalkImageTextSprite.m
//  BarrageRendererDemo
//
//  Created by UnAsh on 15/11/15.
//  Copyright (c) 2015年 ExBye Inc. All rights reserved.
//

#import "BarrageWalkImageTextSprite.h"
#import "MLEmojiLabel.h"

@implementation BarrageWalkImageTextSprite

- (UIView *)bindingView
{
    MLEmojiLabel * label = [[MLEmojiLabel alloc]initWithFrame:CGRectZero];
    
    
    
    NSString *str = [self.text stringByReplacingOccurrencesOfString:@"[love]" withString:@"/::B"];
    
    str = [str stringByReplacingOccurrencesOfString:@"[aha]" withString:@"/::D"];
    
    str = [str stringByReplacingOccurrencesOfString:@"[amaz]" withString:@"/:&-("];
    
    str = [str stringByReplacingOccurrencesOfString:@"[amaz]" withString:@"/:&-("];
    str = [str stringByReplacingOccurrencesOfString:@"[cool]" withString:@"/::+"];
    str = [str stringByReplacingOccurrencesOfString:@"[flower]" withString:@"/:rose"];
    str = [str stringByReplacingOccurrencesOfString:@"[good]" withString:@"/:strong"];
    str = [str stringByReplacingOccurrencesOfString:@"[hard]" withString:@"/:,@f"];
    str = [str stringByReplacingOccurrencesOfString:@"[bye]" withString:@"/:bye"];
    str = [str stringByReplacingOccurrencesOfString:@"[pitiful]" withString:@"/:pd"];
    str = [str stringByReplacingOccurrencesOfString:@"[why]" withString:@"/::-S"];
    
    label.text = str;
    label.textColor = self.textColor;
    label.font = [UIFont systemFontOfSize:self.fontSize];
//    if (self.cornerRadius > 0) {
//        label.layer.cornerRadius = self.cornerRadius;
        label.clipsToBounds = YES;
//    }
    label.layer.borderColor = self.borderColor.CGColor;
    label.layer.borderWidth = self.borderWidth;
    label.backgroundColor = self.backgroundColor;
    
    label.backgroundColor = [UIColor clearColor];
    label.lineBreakMode = NSLineBreakByCharWrapping;
    label.isNeedAtAndPoundSign = YES;
    return label;
}

@end
