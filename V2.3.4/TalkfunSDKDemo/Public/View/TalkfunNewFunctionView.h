//
//  TalkfunNewFunctionView.h
//  TalkfunSDKDemo
//
//  Created by 孙兆能 on 2016/12/14.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TalkfunGradientView;
@class TalkfunNewFunctionButton;
@interface TalkfunNewFunctionView : UIView

@property (nonatomic,copy) void (^sliderValueBlock)(CGFloat sliderValue);
@property (weak, nonatomic) IBOutlet TalkfunGradientView *topView;
@property (weak, nonatomic) IBOutlet TalkfunGradientView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtn;
@property (weak, nonatomic) IBOutlet UIButton *exchangeBtn;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;
@property (weak, nonatomic) IBOutlet UIButton *networkBtn;
@property (weak, nonatomic) IBOutlet UIButton *danmuBtn;
@property (weak, nonatomic) IBOutlet UIButton *fullScreenBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *networkTrailingSpaceToFullScreen;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (nonatomic,assign) BOOL touch;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timelabelLeadingSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderLeadingSpace;
@property (weak, nonatomic) IBOutlet UIView *kuaiJinTuiView;
@property (weak, nonatomic) IBOutlet UIImageView *kuaiImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabelAndTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *kuaiSecondLabel;

+ (id)initView;
- (void)buttonsAddTarget:(id)target action:(SEL)action;
- (void)playbackMode:(BOOL)playbackMode;
- (void)play:(BOOL)play;
- (void)setDuration:(CGFloat)duration;
- (void)totalTimeLabelShow:(BOOL)show;

@end
