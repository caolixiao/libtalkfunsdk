//
//  TalkfunReplyCell.m
//  TalkfunSDKDemo
//
//  Created by Sunzn on 2017/2/5.
//  Copyright © 2017年 talk-fun. All rights reserved.
//

#import "TalkfunReplyCell.h"
#import "UIImageView+WebCache.h"
@implementation TalkfunReplyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.roleLabel.clipsToBounds = YES;
    
    
    self.headPortrait.backgroundColor = [UIColor whiteColor];
    self.headPortrait.layer.cornerRadius = 11;
    //设置裁剪
    self.headPortrait.layer.masksToBounds = YES;
    self.headPortrait.image = [UIImage imageNamed:@"placeholderImage"];
}

- (void)configCell:(NSDictionary *)dict{
    if ([dict[@"role"] isEqualToString:TalkfunMemberRoleSpadmin]) {
        self.roleLabel.text = @"老师";
        self.roleLabelWidth.constant = 27;
        self.roleLabel.layer.cornerRadius = 3;
        self.roleLabel.backgroundColor = [UIColor redColor];
    }
    //=============== 如果是助教说的话 =================
    else if ([dict[@"role"] isEqualToString:TalkfunMemberRoleAdmin])
    {
        self.roleLabel.text = @"助教";
        self.roleLabelWidth.constant = 27;
        self.roleLabel.layer.cornerRadius = 3;
        self.roleLabel.backgroundColor = [UIColor orangeColor];
    }else{
        self.roleLabelWidth.constant = 0;
    }
    self.nameTimeLabel.textColor = LIGHTBLUECOLOR;
    self.nameTimeLabel.text = [TalkfunUtils getUserNameAndTimeWith:dict playback:NO];
    NSString * content = dict[@"content"];
    NSDictionary * contentDict = [TalkfunUtils assembleAttributeString:content boundingSize:CGSizeMake(CGRectGetWidth(self.frame)-48, MAXFLOAT) fontSize:13 shadow:NO];
    NSAttributedString * attr = contentDict[AttributeStr];
    NSMutableAttributedString * contentAttrStr = [[NSMutableAttributedString alloc] initWithAttributedString:attr];
    UIColor * contentColor = [UIColor whiteColor];
    [contentAttrStr addAttribute:NSForegroundColorAttributeName value:contentColor range:NSMakeRange(0, attr.length)];
    [contentAttrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(0, attr.length)];
    self.content.attributedText = contentAttrStr;
    
    
    
    
    NSString *  avatar = dict[@"avatar"];
    //有图片
    if (avatar.length>0) {
        [self.headPortrait sd_setImageWithURL:[NSURL URLWithString:avatar]];
    }else{
        self.headPortrait.image = [UIImage imageNamed:@"placeholderImage"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
