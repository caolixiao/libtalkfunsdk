//
//  UIScrollView+TalkfunScrollView.m
//  TalkfunSDKDemo
//
//  Created by 孙兆能 on 2016/11/19.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import "UIScrollView+TalkfunScrollView.h"

@implementation UIScrollView (TalkfunScrollView)

- (id)initScrollViewWithTarget:(id)target frame:(CGRect)frame
{
    self = [super init];
    if (self) {
        self = [[UIScrollView alloc] initWithFrame:frame];
        
        self.pagingEnabled                  = YES;
        self.backgroundColor                = [UIColor whiteColor];
        self.delegate                       = target;
        self.bounces                        = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator   = NO;
    }
    return self;
}

@end
