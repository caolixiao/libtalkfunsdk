//
//  TalkfunNewFunctionView.m
//  TalkfunSDKDemo
//
//  Created by 孙兆能 on 2016/12/14.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import "TalkfunNewFunctionView.h"
#import "TalkfunGradientView.h"

@implementation TalkfunNewFunctionView

+ (id)initView{
    TalkfunNewFunctionView * newFunctionView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil][0];
    return newFunctionView;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    self.playBtn.hidden = YES;
    self.slider.hidden = YES;
    self.coverBtn.hidden = YES;
    [_topView addLayer];
    [_bottomView addLayer];
    self.fullScreenBtn.selected = NO;
    self.timeLabel.hidden = YES;
    self.totalTimeLabel.hidden = YES;
    [self kuaiHide:YES];
}

- (void)kuaiHide:(BOOL)hide{
    self.kuaiJinTuiView.hidden = hide;
    self.kuaiSecondLabel.hidden = hide;
    self.kuaiImageView.hidden = hide;
    self.timeLabelAndTotalLabel.hidden = hide;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.sliderLeadingSpace.constant = TimeLabelShow?3+CGRectGetWidth(self.timeLabel.frame):3;
    self.timelabelLeadingSpace.constant = TimeLabelShow?0:CGRectGetMinX(self.networkBtn.frame)-CGRectGetMaxX(self.playBtn.frame)-70;
}

- (void)sliderTap:(UITapGestureRecognizer *)sliderTap
{
    if (sliderTap.state == UIGestureRecognizerStateEnded) {
        if (!self.touch) {
            CGPoint location = [sliderTap locationInView:self.slider];
            float x = location.x;
            float r = x / self.slider.frame.size.width;
            float value = (self.slider.maximumValue - self.slider.minimumValue) * r;
            [self.slider setValue:value animated:YES];
        }
    }
    [self sliderValueChange:self.slider];
}

- (void)sliderValueChange:(UISlider *)slider
{
    WeakSelf
    if (self.sliderValueBlock) {
        self.sliderValueBlock(slider.value);
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.touch = NO;
    });
    [self.playBtn setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    self.playBtn.selected = NO;
}

- (void)sliderThumbTouch:(UISlider *)slider
{
    self.touch = YES;
}

- (void)sliderThumbUnTouch:(UISlider *)slider
{
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.touch = NO;
    });
}

- (void)buttonsAddTarget:(id)target action:(SEL)action{
    [self.backBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.cameraBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.exchangeBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.refreshBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.networkBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.danmuBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.fullScreenBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (void)playbackMode:(BOOL)playbackMode{
    if (playbackMode) {
        self.networkTrailingSpaceToFullScreen.constant = 0;
        self.danmuBtn.hidden = YES;
        self.playBtn.hidden = NO;
        self.slider.hidden = NO;
        self.coverBtn.hidden = NO;
        self.timeLabel.hidden = NO;
        UITapGestureRecognizer * sliderTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTap:)];
        [self.slider addGestureRecognizer:sliderTap];
        self.slider.continuous = NO;
        [self.slider addGestureRecognizer:sliderTap];
        self.slider.value = 0.0;
//        self.slider.userInteractionEnabled = NO;
        self.touch = NO;
        [self.slider addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
        [self.slider addTarget:self action:@selector(sliderThumbTouch:) forControlEvents:UIControlEventTouchDown];
        [self.slider addTarget:self action:@selector(sliderThumbUnTouch:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
        [self.coverBtn addSubview:self.slider];
        [self.slider setThumbImage:[UIImage imageNamed:@"进度O"] forState:UIControlStateNormal];
        [self.slider setMinimumTrackImage:[UIImage imageNamed:@"进度2"] forState:UIControlStateNormal];
        [self.slider setMaximumTrackImage:[UIImage imageNamed:@"进度1"] forState:UIControlStateNormal];
    }
}

- (void)play:(BOOL)play{
    [self.playBtn setImage:[UIImage imageNamed:play?@"pause":@"play"] forState:UIControlStateNormal];
    self.playBtn.selected = !play;
}

static BOOL TimeLabelShow = NO;
- (void)totalTimeLabelShow:(BOOL)show{
    TimeLabelShow = show;
    CGFloat duration = self.slider.maximumValue;
    self.totalTimeLabel.text = [self getTimeStr:duration];
    self.totalTimeLabel.hidden = !show;
//    self.timelabelLeadingSpace.constant = show?0:CGRectGetMinX(self.networkBtn.frame)-CGRectGetMaxX(self.playBtn.frame);
}

- (void)setDuration:(CGFloat)duration{
    if (self.touch == NO && fabs(self.slider.value - duration) > 1.0) {
        [self.slider setValue:duration animated:YES];
    }
    self.timeLabel.text = [self getTimeStr:duration];
    CGFloat totalDuration = self.slider.maximumValue;
    self.totalTimeLabel.text = [self getTimeStr:totalDuration];
}

- (void)kuai:(CGFloat)duration hide:(BOOL)hide{
    if (hide) {
        [self kuaiHide:YES];
        return;
    }else{
        [self kuaiHide:NO];
    }
    self.kuaiImageView.image = [UIImage imageNamed:duration>=0?@"快进":@"倒退"];
    NSString * timeLabelStr = self.timeLabel.text;
    NSString * totalTimeStr = [self getTimeStr:self.slider.maximumValue];
    self.timeLabelAndTotalLabel.text = [NSString stringWithFormat:@"%@ / %@",timeLabelStr,totalTimeStr];
    self.kuaiSecondLabel.text = [NSString stringWithFormat:@"%@%d秒",duration>=0?@"+":@"",(int)duration];
}

- (NSString *)getTimeStr:(CGFloat)duration{
    NSInteger hour   = duration / 3600;
    NSInteger minute = (duration - hour * 3600) / 60;
    NSInteger second = duration - minute * 60 - hour * 3600;
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)hour,(long)minute,(long)second];
}

@end
