//
//  MyLotteryViewController.m
//  TalkfunSDKDemo
//
//  Created by talk－fun on 16/2/29.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import "MyLotteryViewController.h"

@interface MyLotteryViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation MyLotteryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.view.alpha = 1.0;
}

- (void)refreshUIWithInfo:(NSDictionary *)info
{
    NSArray * infoArr   = info[@"result"];

    NSString * nickName = infoArr.firstObject[@"nickname"];
    self.nameLabel.text = nickName;
}


- (IBAction)deleteBtnClicked:(UIButton *)sender {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
    }];
    
}



@end
