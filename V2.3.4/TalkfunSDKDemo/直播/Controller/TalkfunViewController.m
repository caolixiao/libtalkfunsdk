//
//  TalkfunViewController.m
//  TalkfunSDKDemo
//
//  Created by 孙兆能 on 2016/11/18.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import "TalkfunViewController.h"
#import "TalkfunSDK.h"
#import "UIButton+TalkfunButton.h"
#import "UILabel+TalkfunLabel.h"
#import "UIScrollView+TalkfunScrollView.h"
#import "UIView+TalkfunView.h"
#import "RootTableViewController.h"
#import "UITextField+TalkfunTextField.h"
#import "UIImageView+TalkfunImageView.h"
#import "NetworkSelectionViewController.h"
#import "VoteViewController.h"
#import "VoteEndViewController.h"
#import "LotteryViewController.h"
#import "MyLotteryViewController.h"
#import "TalkfunMessageView.h"
#import "TalkfunFunctionView.h"
#import "TalkfunHornView.h"
#import "TalkfunNetworkStatusView.h"
#import "BarrageRenderer.h"
#import "BarrageWalkImageTextSprite.h"
#import "TalkfunTextfieldView.h"
#import "TalkfunNewFunctionView.h"
#import "TalkfunButtonView.h"
#import "TalkfunNewTextfieldView.h"
#import "TalkfunExpressionViewController.h"
#import "TalkfunLongTextfieldView.h"
#import "TalkfunReplyTipsView.h"
#import "TalkfunGuidanceView.h"
#import "GroupTableViewController.h"
#import "HYAlertView.h"
#define TrailingValue 40
#define NetworkStatusViewWidth 147
#define ButtonViewHeight 35
#define UpSpace 40
#define success @"0"

@interface TalkfunViewController ()<UIScrollViewDelegate,UITextFieldDelegate,BarrageRendererDelegate,UIAlertViewDelegate>



//SDK
@property (nonatomic,strong) TalkfunSDK * talkfunSDK;
@property (nonatomic,strong) UIView * pptView;
@property (nonatomic,strong) UIView * cameraView;
//桌面分享(可选、默认pptView为desktopShareView)
@property (nonatomic,strong) UIView * desktopShareView;

//滚动通知的东西
@property (nonatomic,copy) NSString * link;

//ppt信息及按钮
@property (nonatomic,strong) TalkfunNewFunctionView * pptsFunctionView;
@property (nonatomic,strong) TalkfunHornView * hornView;

//btnView
@property (nonatomic,strong) TalkfunButtonView * buttonView;

//scrollView及其上面的东西
@property (nonatomic,strong) TalkfunNewTextfieldView * chatTFView;
@property (nonatomic,strong) TalkfunTextfieldView * askTFView;
//横屏时候发送聊天的TF
@property (nonatomic,strong) TalkfunLongTextfieldView * longTextfieldView;
@property (nonatomic,strong) TalkfunNewTextfieldView * longCoverTFView;
@property (nonatomic,strong) UIScrollView * scrollView;
@property (nonatomic,strong) UIView * shadowView;
@property (nonatomic,strong) TalkfunExpressionViewController * expressionViewVC;
//投票
@property (nonatomic,strong) VoteViewController     * voteVC;
@property (nonatomic,strong) VoteEndViewController   * voteEndVC;
@property (nonatomic,strong) LotteryViewController   * lotteryVC;
@property (nonatomic,strong) MyLotteryViewController * myLotteryVC;
//记住投票信息的字典
@property (nonatomic,strong) NSMutableDictionary * voteNewDict;
@property (nonatomic,strong) NSMutableDictionary * votePubDict;
//记住已经投了票的vid
@property (nonatomic,strong) NSMutableArray      * voteFinishArray;

//老师回复提示
@property (nonatomic,strong) TalkfunReplyTipsView * replyTipsView;
//网络较差提示
@property (nonatomic,strong) UIImageView * networkTipsImageView;

//私聊
@property (nonatomic,strong) UIButton                 * groupBtn;
@property (nonatomic,strong) GroupTableViewController * groupTableVC;
@property (nonatomic,strong) UINavigationController   * nav;
@property (nonatomic,strong) NSMutableDictionary      * chatGroupsDict;

//送花
@property (nonatomic,strong) UILabel * flowerTipsLabel;

//弹幕
@property (nonatomic,strong) BarrageRenderer * barrageRender;
@property (nonatomic,strong) NSDate * startTime;//开始时间

//被强迫下线
@property (nonatomic,strong) UIAlertView * forceoutAlertView;
//被提出房间
@property (nonatomic,strong) UIAlertView * kickoutAlertView;
//视频切换、暂停提示
@property (nonatomic,strong) UILabel * tipsLabel;
//网络状态
@property (nonatomic,assign) TalkfunNetworkStatus networkStatus;
//网络选择懒加载
@property (nonatomic,strong) NetworkSelectionViewController * networkSelectionVC;
//其它
//横竖屏
@property (nonatomic,assign) BOOL isOrientationLandscape;
//是否已经开始直播
@property (nonatomic,assign) BOOL unStart;
//退出确认alertController
@property (nonatomic,strong) UIAlertView * quitAlertView;
//是否切换了摄像头和ppt容器
@property (nonatomic,assign) BOOL isExchanged;
//方向
@property (nonatomic,assign) NSInteger orientation;
//tableView的数量
@property (nonatomic,assign) NSInteger tableViewNum;
//覆盖的View（遮布）
@property (nonatomic,strong) UIView * coverView;
//网络状态偏出的位置
@property (nonatomic,assign) CGFloat extraValue;
//记住自己信息的字典
@property (nonatomic,strong) NSDictionary * me;
//是否第一次进来
@property (nonatomic,assign) BOOL isFirst;
//计时的time
@property (nonatomic,assign) NSInteger time;
//是否是iPad且自动旋转
@property (nonatomic,assign) BOOL iPadAutoRotate;

//输入时的透明背景黑色
@property (nonatomic,strong) UIView * inputBackgroundView;
//消失网络差的提示timer
@property (nonatomic,strong) NSTimer * dismissTimer;

//是否是桌面分享
@property (nonatomic,assign)BOOL isDesktop;

//所有人禁言
@property (nonatomic,assign)BOOL disableAll;

@property (nonatomic,strong)HYAlertView *alertView ;
@end

@implementation TalkfunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    //隐藏状态栏
    [APPLICATION setStatusBarHidden:YES];
    self.unStart = YES;
    self.isFirst = YES;
    self.isExchanged = NO;
    self.isDesktop = NO;
    self.isOrientationLandscape = NO;
    self.iPadAutoRotate = YES;
    self.isDesktop = NO;
    //开始
    [self getAccessToken];
    
    [self createUI];
    
    [self addGesture];
    
    [self onSomething];
    
    //=================== 监听键盘 ====================
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    //=================== 监听输入框的字符长度 ====================
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(infoAction:)name:UITextFieldTextDidChangeNotification object:nil];
    if (IsIPAD) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    self.orientation = 3;
}

#pragma mark - 实例化talkfunSDK
- (void)getAccessToken
{
    NSString * token = nil;
    if (_res) {
        token = _res[@"data"][@"access_token"];
    }
    else
    {
        token = _token;
    }
    if (token && ![token isEqualToString:@""]) {
        [self configViewWithAccessToken:token];
    }
    else
    {
        WeakSelf
        [self.view toast:@"token不能为空" position:ToastPosition];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            QUITCONTROLLER(weakSelf)
        });
    }
}

//SDK初始化基本东西
- (void)configViewWithAccessToken:(NSString *)access_token
{
    //属性字典
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    //是否是点播
    parameters[TalkfunIsPlayback]     = @(NO);
    //    parameters[TalkfunAuthentication] = @(TalkfunAuthenticationByAccessKey);
    
    //1.实例化SDK
    self.talkfunSDK = [[TalkfunSDK alloc] initWithAccessToken:access_token parameters:parameters];
    
    //进入后台是否暂停（默认是暂停）
    [self.talkfunSDK setPauseInBackground:NO];
    
    //设置url白名单
    [self.talkfunSDK setDomainWhitelist:@[@"172.16.117.235:7786"]];
    
    //ppt容器（4：3比例自适应）
    self.pptView = ({
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSize.width, self.view.frame.size.width * 3 / 4.0)];
        view.backgroundColor = [UIColor blackColor];
        [self.view addSubview:view];
        view;
    });
    
    //2.把ppt容器给SDK（要显示ppt区域的必须部分）
    [self.talkfunSDK configurePPTContainerView:self.pptView];
    
    //cameraView容器
    self.cameraView = ({
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(ScreenSize.width - 150, CGRectGetMaxY(self.pptView.frame) + ButtonViewHeight, 150, 150 * 3 / 4.0)];
        if (IsIPAD) {
            view.frame = CGRectMake(ScreenSize.width * 0.7, CGRectGetMaxY(self.pptView.frame) + ButtonViewHeight, ScreenSize.width * 0.3, ScreenSize.width * 0.3 * 3 / 4.0);
        }
        view.backgroundColor = [UIColor blackColor];
        //首先把容器隐藏
        view.hidden = YES;
        view;
    });
    
    //3.把ppt容器给SDK（要显示摄像头区域的必须部分）
    [self.talkfunSDK configureCameraContainerView:self.cameraView];
    
    //桌面分享(可选、默认pptView为desktopShareView)
    //    self.desktopShareView = ({
    //        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.pptView.frame), CGRectGetHeight(self.pptView.frame))];
    //        [self.talkfunSDK configureDesktopContainerView:view];
    //        [self.pptView addSubview:view];
    //        view;
    //    });
}

- (void)createUI{
    [self createPPTsButton];
    self.pptsFunctionView.hidden = NO;
    [self createScrollView];
    //加btnView
    [self.view addSubview:self.buttonView];
    [self.view addSubview:self.cameraView];
    _startTime = [NSDate date];
    [self.barrageRender start];
    _barrageRender.speed = 0.5;
    [self.pptsFunctionView insertSubview:self.inputBackgroundView atIndex:0];
    
    self.expressionViewVC.view.frame = CGRectMake(0, ScreenSize.height, ScreenSize.width,ExpressionViewHeight());
    
    [self.view addSubview:self.expressionViewVC.view];
    [self addGuidance];
}

- (void)addGuidance{
    TalkfunGuidanceView * gv = [[TalkfunGuidanceView alloc] initView];
    gv.frame = self.view.frame;
    [self.view addSubview:gv];
}

#pragma mark - PPT上的功能按钮
- (void)createPPTsButton{
    [self.pptView addSubview:self.pptsFunctionView];
}

- (void)addNetworkTipsView{
    if (_networkTipsImageView) {
        return;
    }
    [self.dismissTimer invalidate];
    [self.view addSubview:self.networkTipsImageView];
    self.dismissTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(networkTipsTap:) userInfo:nil repeats:NO];
}
- (void)addReplyTipsView{
    if (_replyTipsView) {
        return;
    }
    [self.view addSubview:self.replyTipsView];
}

#pragma mark - 创建scrollView
- (void)createScrollView{
    [self.view addSubview:self.shadowView];
    [self.view addSubview:self.scrollView];
    
    /*=================加tableView===================*/
    NSArray * tableVCName = @[@"ChatTableViewController",
                              @"QuestionTableViewController",
                              @"NoticeTableViewController"];
    
    self.tableViewNum = tableVCName.count;
    for (int i = 0; i < self.tableViewNum; i ++) {
        
        Class className = NSClassFromString(tableVCName[i]);
        
        RootTableViewController * tableViewVC = [[className alloc] init];
        tableViewVC.view.frame = CGRectMake(i * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height - 50);
        
        WeakSelf
        //聊天页面
        if (i == 0) {
            tableViewVC.btnBlock = ^(BOOL voted,NSString *vid){
                //点击 查看结果 按钮
                if (voted) {
                    [weakSelf.talkfunSDK emit:@"vote:get:data" parameter:@{@"vid":vid} callback:^(id obj) {
                        
                        weakSelf.voteVC.view.alpha = 1.0;
                        [weakSelf.voteVC removeFromSuperview];
                        
                        [weakSelf.voteVC refreshUIWithResult:obj[@"data"]];
                        [weakSelf.view addSubview:weakSelf.voteVC.view];
                    }];
                    return ;
                }
                
                if (weakSelf.votePubDict[vid]) {
                    weakSelf.voteEndVC.voteTitle.text = @"投票已结束";
                    weakSelf.voteEndVC.message = @"投票已结束";
                    weakSelf.voteEndVC.view.alpha = 1.0;
                    [weakSelf.view addSubview:weakSelf.voteEndVC.view];
                    [weakSelf.voteEndVC refreshUIWithAfterCommitted];
                    return;
                }
                else if ([weakSelf.voteFinishArray containsObject:vid])
                {
                    weakSelf.voteEndVC.view.alpha = 1.0;
                    [weakSelf.view addSubview:weakSelf.voteEndVC.view];
                    [weakSelf.voteEndVC refreshUIWithAfterCommitted];
                }else if (!voted){
                    if (weakSelf.votePubDict[vid]) {
                        weakSelf.voteVC.view.alpha = 1.0;
                        [weakSelf.voteVC removeFromSuperview];
                        [weakSelf.view addSubview:weakSelf.voteVC.view];
                        id obj = weakSelf.votePubDict[vid];
                        
                        [weakSelf.voteVC refreshUIWithResult:obj];
                    }else{
                        //
                        //                        [weakSelf.view addSubview:weakSelf.voteVC.view];
                        ;
                        id obj = weakSelf.voteNewDict[vid];
                        [weakSelf.voteVC removeFromSuperview];
                        [weakSelf.voteVC refreshUIWithParams:obj];
                        
                        weakSelf.voteVC.view.alpha = 1.0;
                        [weakSelf.view addSubview:weakSelf.voteVC.view];
                    }
                }
            };
        }
        //公告
        if (i == 2) {
            tableViewVC.view.frame = CGRectMake(i * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        }
        
        tableViewVC.view.tag = 300 + i;
        tableViewVC.view.backgroundColor = DARKBLUECOLOR;
        [self.scrollView addSubview:tableViewVC.view];
        [self addChildViewController:tableViewVC];
    }
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.scrollView.frame) * self.tableViewNum, CGRectGetHeight(self.scrollView.frame));
    /*=================添加textfield===================*/
    [self.scrollView addSubview:self.chatTFView];
    [self.scrollView addSubview:self.askTFView];
}

#pragma mark - 监听事件
- (void)onSomething
{
    WeakSelf
    //网速
    [self.talkfunSDK on:@"network:speed" callback:^(id obj) {
        [weakSelf networkSpeed:obj];
    }];
    
    //TODO:桌面分享
    [self.talkfunSDK on:@"live:desktop:start" callback:^(id obj) {
        weakSelf.isDesktop = YES;
        [weakSelf.cameraView removeFromSuperview];
    }];
    
    [self.talkfunSDK on:@"live:desktop:stop" callback:^(id obj) {
        weakSelf.isDesktop = NO;
        [weakSelf.view addSubview:weakSelf.cameraView];
    }];
    
    [self.talkfunSDK on:@"live:desktop:pause" callback:^(id obj) {
        weakSelf.tipsLabel.text = @"暂停中......";
        [weakSelf.pptView addSubview:weakSelf.tipsLabel];
    }];
    
    [self.talkfunSDK on:@"camera:show" callback:^(id obj) {
        weakSelf.cameraView.hidden = NO;
        if (IsIPAD&&[UIApplication sharedApplication].statusBarOrientation!=UIInterfaceOrientationPortrait&&weakSelf.pptsFunctionView.fullScreenBtn.selected==NO) {
            [weakSelf reloadScrollView:NO];
        }
    }];
    
    [self.talkfunSDK on:@"camera:hide" callback:^(id obj) {
        weakSelf.cameraView.hidden = YES;
        if (IsIPAD&&[UIApplication sharedApplication].statusBarOrientation!=UIInterfaceOrientationPortrait&&weakSelf.pptsFunctionView.fullScreenBtn.selected==NO) {
            [weakSelf reloadScrollView:YES];
        }
    }];
    
    //视频切换
    [self.talkfunSDK on:@"live:mode:change" callback:^(id obj) {
        [weakSelf liveModeChange:obj];
    }];
    
    //当时视频播放的时候的回调
    [self.talkfunSDK on:@"play" callback:^(id obj) {
        [weakSelf.tipsLabel removeFromSuperview];
        weakSelf.tipsLabel = nil;
    }];
    
    
    //投票
    //投票开始
    [self.talkfunSDK on:@"vote:new" callback:^(id obj) {
        [weakSelf voteNew:obj];
    }];
    //投票结束
    [self.talkfunSDK on:@"vote:pub" callback:^(id obj) {
        [weakSelf votePub:obj];
    }];
    //开始抽奖
    [self.talkfunSDK on:@"lottery:start" callback:^(id obj) {
        [weakSelf lotteryStart:obj];
    }];
    //结束抽奖
    [self.talkfunSDK on:@"lottery:stop" callback:^(id obj) {
        [weakSelf lotteryStop:obj];
    }];
    
    //广播
    [self.talkfunSDK on:@"broadcast" callback:^(id obj) {
        NSString * mess    = obj[@"message"];
        NSString * message = [NSString stringWithFormat:@"公共广播: %@",mess];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"broadcast":message,@"mess":mess}];
    }];
    
    //禁言
    [self.talkfunSDK on:@"chat:disable" callback:^(id obj) {
        [weakSelf chatDisable:obj];
    }];
    
    //全体禁言
    [self.talkfunSDK on:@"chat:disable:all" callback:^(id obj) {
        [weakSelf chatDisableAll:obj];
    }];
    
    //发送聊天
    [self.talkfunSDK on:@"chat:send" callback:^(id obj) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"chat" object:nil userInfo:@{@"mess":obj}];
        NSString *text =  obj[@"msg"];
        //加载数据
        [weakSelf loadData:text];
    }];
    
    //老师删除信息
    [self.talkfunSDK on:@"question:delete" callback:^(id obj) {
        NSString * qid = [NSString stringWithFormat:@"%@",obj[@"qid"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteQuestion" object:nil userInfo:@{@"qid":qid}];
    }];
    
    //TODO:送花
    [self.talkfunSDK on:@"flower:send" callback:^(id obj) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"flower" object:nil userInfo:@{@"mess":obj}];
    }];
    [self.talkfunSDK on:@"flower:get:init" callback:^(id obj) {
        [weakSelf flowerGetInit:obj];
    }];
    [self.talkfunSDK on:@"flower:total" callback:^(id obj) {
        [weakSelf flowerTotal:obj];
    }];
    [self.talkfunSDK on:@"flower:time:left" callback:^(id obj) {
        //[weakSelf flowerTimeLeft:obj];
    }];
    
    //问题列表
    [self.talkfunSDK on:@"question:list" callback:^(id obj) {
        if (weakSelf.unStart) {
            //weakSelf.messageView.liveStatusLabel.text = @"直播未开始";
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"questionList" object:nil userInfo:@{@"mess":obj,@"xid":weakSelf.me}];
    }];
    
    //提问
    [self.talkfunSDK on:@"question:ask" callback:^(id obj) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ask" object:nil userInfo:@{@"mess":obj}];
    }];
    
    //问题回答
    [self.talkfunSDK on:@"question:reply" callback:^(id obj) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reply" object:nil userInfo:@{@"mess":obj}];
        if (weakSelf.scrollView.contentOffset.x!= ScreenSize.width&&[obj[@"question"][@"xid"] integerValue]==[weakSelf.me[@"xid"] integerValue] && weakSelf.pptsFunctionView.fullScreenBtn.selected == NO) {
            PERFORM_IN_MAIN_QUEUE([weakSelf addReplyTipsView];)
        }
    }];
    
    //有人加入直播间
    [self.talkfunSDK on:@"member:join:other" callback:^(id obj) {
        [weakSelf memberJoinOther:obj];
    }];
    
    //有人退出房间
    [self.talkfunSDK on:@"member:leave" callback:^(id obj) {
        [weakSelf memberLeave:obj];
    }];
    
    //公告
    [self.talkfunSDK on:@"announce:notice" callback:^(id obj) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notice" object:nil userInfo:@{@"mess":obj}];
    }];
    
    //我加入房间
    [self.talkfunSDK on:@"member:join:me" callback:^(id obj) {
    }];
    
    //直播开始
    [self.talkfunSDK on:@"live:start" callback:^(id obj) {
        [weakSelf liveStart:obj];
    }];
    
    //直播结束
    [self.talkfunSDK on:@"live:stop" callback:^(id obj) {
        [weakSelf liveStop:obj];
    }];
    
    //被管理员踢出房间
    [self.talkfunSDK on:@"member:kick" callback:^(id obj) {
        [weakSelf memberKick:obj];
    }];
    
    //被强迫下线
    [self.talkfunSDK on:@"member:forceout" callback:^(id obj) {
        [weakSelf memberForceout:obj];
    }];
    
    //滚动通知
    [self.talkfunSDK on:@"announce:roll" callback:^(id obj) {
        [weakSelf.hornView announceRoll:obj];
    }];
    
    //MARK:初始化(room:init)
    [self.talkfunSDK on:@"room:init" callback:^(id obj) {
        [weakSelf roomInit:obj];
    }];
    
    //总人数
    [self.talkfunSDK on:@"member:total" callback:^(id obj) {
        [weakSelf memberTotal:obj];
    }];
    
    //================= 未读消息红点 ====================
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatMessageCome) name:@"chatMessageCome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(askMessageCome) name:@"askMessageCome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeMessageCome) name:@"noticeMessageCome" object:nil];
}

- (void)btnViewButtonsClicked:(UIButton *)button{
    [self.buttonView selectButton:(TalkfunNewButtonViewButton *)button];
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width * (button.tag - 500), 0) animated:NO];
    if (button.tag == 501) {
        [self removeReplyTipsView];
    }else if (button.tag == 502) {
        [self.view endEditing:YES];
    }
    //    {
    //        info =     {
    //            answer = "";
    //            gid = 0;
    //            imageUrl = "https://lp2-4.talk-fun.com/openimg/vote/2017/02/1488295067.png";
    //            label = zxcc;
    //            nickname = "\U83ab\U745e\U6743";
    //            noticeTime = "09:07";
    //            role = spadmin;
    //            startTime = "2017-02-28 09:07:07";
    //            title = zxczxc;
    //            xid = 5662896;
    //        };
    //        opList =     (
    //                      z,
    //                      x,
    //                      c
    //                      );
    //        optional = 1;
    //        vid = 17855;
    //    }
    
    //    if (button.tag==101) {
    //        NSDictionary *dict = @{
    //                               @"optional" : @"1",
    //                               @"vid" : @"17855",
    //                               @"opList":@[@"1", @"2", @"3", @15],
    //
    //                                @"info":@{
    //                                        @"answer":@"",
    //                                        @"gid":@"0",
    //                                        @"imageUrl":@"https://lp2-4.talk-fun.com/openimg/vote/2017/03/1488383200.png",
    //                                        @"label" : @"zxcc",
    //                                        @"nickname" : @"wode",
    //                                        @"noticeTime" : @"09:07",
    //                                        @"role" : @"spadmin",
    //                                        @"startTime" : @"2017-02-28 09:07:07",
    //                                        @"title" : @"zxczxc",
    //                                        @"xid" : @"5662896"
    //                                        },
    //                                };
    //
    //
    //         [self voteNew:dict];
    //    }
    //   else if (button.tag==102) {
    ////        NSDictionary *dict = @{
    ////                               @"optional" : @"1",
    ////                               @"vid" : @"17855",
    ////                               @"opList":@[@"1", @"2", @"3", @15],
    ////
    ////                               @"info":@{
    ////                                       @"answer":@"",
    ////                                       @"gid":@"0",
    ////                                       @"imageUrl":@"",
    ////                                       @"label" : @"zxcc",
    ////                                       @"nickname" : @"wode",
    ////                                       @"noticeTime" : @"09:07",
    ////                                       @"role" : @"spadmin",
    ////                                       @"startTime" : @"2017-02-28 09:07:07",
    ////                                       @"title" : @"zxczxc",
    ////                                       @"xid" : @"5662896"
    ////                                       },
    ////                               };
    ////
    ////
    ////        [self voteNew:dict];
    //
    //
    ////               NSDictionary *dict = @{
    ////                                      @"optional" : @"1",
    ////                                      @"vid" : @"17855",
    ////                                      @"opList":@[@"1", @"2", @"3", @15],
    ////
    ////                                      @"info":@{
    ////                                          @"uid":@"zb_fc50c229e062eaeb7d7668950cd47296",
    ////
    ////                                              @"bid":@"14035",
    ////                                              @"title" : @"hfg",
    ////                                              @"label" : @"dfsdf",
    ////                                              @"optional" : @"1",
    ////
    ////                                          @" answer" : @"",
    ////                                          @"vid" : @"18268",
    ////                                          @"noticeTime" : @"18:58",
    ////                                          @"owner" : @"1",
    ////                                          @"imageUrl" : @"",
    ////                                          @"startTime" : @"03-14 18:58",
    ////                                          @"role": @"spadmin",
    ////                                          @"nickname" : @"江如彬",
    ////                                          @"endTime" : @"2017-03-14 18:58:30",
    ////                                          @"status" : @"2",
    ////
    ////                                              },
    ////
    ////                                      @"statsList" : @[
    ////                                                   @{
    ////                                                     @"op" : @"gfh",
    ////                                                       @"opNum" : @"0",
    ////                                                       @"percent" : @"0",
    ////                                                   },
    ////                                                      @{
    ////                                                        @"op" : @"gfh",
    ////                                                        @"opNum" : @"0",
    ////                                                        @"percent" : @"0",
    ////                                                        },
    ////                                                      @{
    ////                                                        @"op" : @"gfh",
    ////                                                        @"opNum" : @"0",
    ////                                                        @"percent" : @"0",
    ////                                                        }
    ////                                                      ],
    ////
    ////                                                    @"isShow": @"1",
    ////
    ////                                      };
    ////       [self votePub:dict];
    //
    //
    //    }
    //
    
    //
    
    
}
- (void)barrageButtonClicked:(UIButton *)button{
    button.selected = !button.selected;
    if (!button.selected) {
        [_barrageRender start];
    }else{
        [_barrageRender stop];
    }
}
- (void)chatButtonClicked:(UIButton *)chatButton
{
    if (self.unStart) {
        [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
        [self.view toast:@"还没开始上课" position:ToastPosition];
        return;
    }
    
    NSMutableDictionary * parameter = [NSMutableDictionary new];
    if (chatButton == self.chatTFView.flowerButton || chatButton == _longCoverTFView.flowerButton) {
        [self.talkfunSDK emit:@"flower:post:send" parameter:parameter callback:nil];
    }else if (chatButton == self.chatTFView.sendButton || chatButton == self.longTextfieldView.sendBtn || chatButton == _longCoverTFView.sendButton)
    {
        if (chatButton == self.chatTFView.sendButton || chatButton == _longCoverTFView.sendButton) {
            parameter[@"msg"] = self.chatTFView.tf.text;
        }else{
            parameter[@"msg"] = self.longTextfieldView.tf.text;
        }
        WeakSelf
        [self.talkfunSDK emit:@"chat:send" parameter:parameter callback:^(id obj) {
            
            if ([obj[@"code"] intValue] == TalkfunCodeSuccess) {
                [weakSelf loadData:parameter[@"msg"]];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"chat" object:nil userInfo:@{@"mess":obj[@"data"]}];
            }
            else if ([obj[@"code"] intValue] == TalkfunCodeFailed)
            {
                [weakSelf.view toast:obj[@"msg"] position:ToastPosition];
            }
            else
            {
                [weakSelf.view toast:@"请说点什么吧。" position:ToastPosition];
            }
        }];
    }
    self.chatTFView.tf.text = nil;
    _longCoverTFView.tf.text = nil;
    self.longTextfieldView.tf.text = nil;
    [self expressionsViewShow:NO];
    [self.chatTFView showSendButton:NO];
    [_longCoverTFView showSendButton:NO];
    [self.view endEditing:YES];
}
//MARK:提问按钮点击事件
- (void)askButtonClicked:(UIButton *)button
{
    if (self.unStart) {
        [self.view endEditing:YES];
        [self.view toast:@"还没开始上课" position:ToastPosition];
        return;
    }
    WeakSelf
    [self.talkfunSDK emit:@"question:ask" parameter:@{@"msg":self.askTFView.myTextField.text} callback:^(id obj) {
        
        if ([obj[@"code"] intValue] == 1202) {
            [weakSelf.askTFView.myTextField resignFirstResponder];
            [weakSelf.view toast:@"当前不在直播中" position:ToastPosition];
        }
        else if (![obj[@"msg"] isEqualToString:@""]) {
            [weakSelf.view toast:obj[@"msg"] position:ToastPosition];
        }
    }];
    
    self.askTFView.myTextField.text = nil;
    [self.view endEditing:YES];
}
//MARK:表情按钮点击事件
- (void)expressionsBtnClicked:(UIButton *)expressionsBtn
{
    expressionsBtn.selected = !expressionsBtn.selected;
    if (expressionsBtn == self.chatTFView.expressionButton || expressionsBtn == _longCoverTFView.expressionButton) {
        //        [UIView animateWithDuration:0.25 animations:^{
        if (expressionsBtn.selected) {
            [self.view endEditing:YES];
            [self expressionsViewShow:YES];
        }else{
            [self expressionsViewShow:NO];
            if (IsIPAD && [UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait) {
                [_longCoverTFView.tf becomeFirstResponder];
            }else
                [self.chatTFView.tf becomeFirstResponder];
        }
        //        }];
    }
    else if (expressionsBtn == self.longTextfieldView.expressionBtn){
        [UIView animateWithDuration:0.25 animations:^{
            if (expressionsBtn.selected) {
                [self.view endEditing:YES];
                [self expressionsViewShow:YES];
            }else{
                [self expressionsViewShow:NO];
                [self.longTextfieldView.tf becomeFirstResponder];
            }
        }];
    }
}

- (void)expressionsViewShow:(BOOL)show{
    if (!self.pptsFunctionView.fullScreenBtn.selected) {
        CGRect rect = _longCoverTFView.frame;
        rect.origin.x = show?0:CGRectGetMaxX(self.pptView.frame);
        rect.size.width = show?ScreenSize.width:CGRectGetWidth(self.scrollView.frame);
        _longCoverTFView.frame = rect;
        if (show) {
            
        }
    }
    [UIView animateWithDuration:0.25 animations:^{
        if (show) {
            _longCoverTFView.transform = CGAffineTransformMakeTranslation(0, -self.expressionViewVC.collectionView.contentSize.height);
            self.longTextfieldView.frame = CGRectMake(0, CGRectGetHeight(self.pptView.frame)-50, CGRectGetWidth(self.pptView.frame), 50);
            self.expressionViewVC.view.transform = CGAffineTransformMakeTranslation(0, -self.expressionViewVC.collectionView.contentSize.height);
            self.longTextfieldView.transform = CGAffineTransformMakeTranslation(0, -self.expressionViewVC.collectionView.contentSize.height);
            self.chatTFView.transform = CGAffineTransformMakeTranslation(0, -self.expressionViewVC.collectionView.contentSize.height);
            if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && self.pptsFunctionView.fullScreenBtn.selected) {
                self.inputBackgroundView.hidden = NO;
            }
            //            if (!self.cameraView.hidden) {
            self.cameraView.transform = CGAffineTransformMakeTranslation(0, (self.view.bounds.size.height- CGRectGetHeight(self.expressionViewVC.view.frame)<CGRectGetMaxY(self.cameraView.frame))?-(CGRectGetMaxY(self.cameraView.frame)-self.view.bounds.size.height+ CGRectGetHeight(self.expressionViewVC.view.frame)):0);
            //            }
        }else{
            self.expressionViewVC.view.transform = CGAffineTransformIdentity;
            self.longTextfieldView.transform = CGAffineTransformIdentity;
            self.chatTFView.transform = CGAffineTransformIdentity;
            _longCoverTFView.transform = CGAffineTransformIdentity;
            self.inputBackgroundView.hidden = YES;
            self.longTextfieldView.frame = CGRectMake(0, CGRectGetHeight(self.pptView.frame)-50, CGRectGetWidth(self.pptView.frame)/2.0, 50);
            self.cameraView.transform = CGAffineTransformIdentity;
        }
        [self.chatTFView expressionBtnSelected:show];
        [_longCoverTFView expressionBtnSelected:show];
        [self.longTextfieldView expressionBtnSelected:show];
    }];
}

#pragma mark - 强制横竖屏
- (void)orientationPortrait
{
    @synchronized (self) {
        
        //先记住转之前的statusBar的方向
        self.orientation = APPLICATION.statusBarOrientation;
        
        //如果现在的statusBar不是竖直方向就旋转
        if (APPLICATION.statusBarOrientation != UIInterfaceOrientationPortrait) {
            CGFloat duration = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
            [UIView animateWithDuration:duration animations:^{
                [APPLICATION setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
                self.view.transform = CGAffineTransformRotate(self.view.transform, - M_PI_2);
            }];
        }
        [APPLICATION setStatusBarHidden:YES];
        self.view.bounds = CGRectMake(0, 0, ScreenSize.width, ScreenSize.height);
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
        [self orientationPortrait:YES];
    }
}

- (void)deviceOrientationChange:(NSNotification *)notification{
    if (self.pptsFunctionView.fullScreenBtn.selected || [self.view.subviews.lastObject isKindOfClass:[TalkfunGuidanceView class]]) {
        return;
    }
    NSLog(@"oooo:%ld",[UIDevice currentDevice].orientation);
    if ([UIDevice currentDevice].orientation == 3 && !self.isOrientationLandscape) {
        [self expressionsViewShow:NO];
        [self.view endEditing:YES];
        [self orientationLandscape];
    }else if ([UIDevice currentDevice].orientation==1 && self.isOrientationLandscape){
        [self expressionsViewShow:NO];
        [self.view endEditing:YES];
        [self orientationPortrait];
    }
}

- (void)orientationPortrait:(BOOL)portrait
{
    self.isOrientationLandscape = !portrait;
    self.alertView.isOrientationLandscape = self.isOrientationLandscape;

    //ppt
    self.pptView.frame = portrait?CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width * 3 / 4.0):IsIPAD&&self.iPadAutoRotate?CGRectMake(0, 0, self.view.bounds.size.width * 7 / 10, self.view.bounds.size.height):CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    self.longTextfieldView.frame = CGRectMake(0, CGRectGetHeight(self.pptView.frame)-50, CGRectGetWidth(self.pptView.frame)/2.0, 50);
    self.inputBackgroundView.frame = self.view.bounds;
    self.longTextfieldView.hidden = portrait|self.iPadAutoRotate;
    self.expressionViewVC.view.frame = CGRectMake(0, ScreenSize.height, ScreenSize.width,ExpressionViewHeight());
    
    //改变喇叭滚动条的frame
    self.hornView.frame = CGRectMake(0, CGRectGetMaxY(self.pptView.frame) - 25, CGRectGetWidth(self.pptView.frame), 25);
    
    //加动画
    [self.hornView rollLabelAddAnimation];
    
    //修改摄像头的frame
    self.cameraView.transform = CGAffineTransformIdentity;
    
    if (IsIPAD) {
        self.cameraView.frame = portrait?CGRectMake(ScreenSize.width * 0.7, CGRectGetMaxY(self.pptView.frame) + ButtonViewHeight, ScreenSize.width*0.3, ScreenSize.width*0.3*3.0/4.0):self.iPadAutoRotate?CGRectMake(CGRectGetMaxX(self.pptView.frame), 0, CGRectGetWidth(self.view.bounds)-CGRectGetWidth(self.pptView.frame), (ScreenSize.width-CGRectGetWidth(self.pptView.frame))*3.0/4.0):CGRectMake(CGRectGetMaxX(self.pptView.frame) - CGRectGetHeight(self.view.bounds)*3.0/10.0, CGRectGetMaxY(self.pptView.frame)-50-(CGRectGetHeight(self.view.bounds)*3.0/10.0)*3.0/4.0, CGRectGetHeight(self.view.bounds)*3.0/10.0, (CGRectGetHeight(self.view.bounds)*3.0/10.0)*3.0/4.0);
    }else{
        CGRect frame = self.cameraView.frame;
        frame.origin = portrait?CGPointMake(ScreenSize.width - 150, CGRectGetMaxY(self.pptView.frame) + ButtonViewHeight):CGPointMake(CGRectGetMaxX(self.pptView.frame) - self.cameraView.frame.size.width, CGRectGetMaxY(self.pptView.frame)-50-CGRectGetHeight(frame));
        self.cameraView.frame = frame;
    }
    //    self.cameraView.backgroundColor = [UIColor blackColor];
    //    self.cameraView.hidden = NO;
    self.buttonView.frame = portrait?CGRectMake(0, CGRectGetMaxY(self.pptView.frame), ScreenSize.width, ButtonViewHeight):CGRectMake(CGRectGetMaxX(self.pptView.frame), self.cameraView.hidden?0:CGRectGetMaxY(self.cameraView.frame), ScreenSize.width-CGRectGetWidth(self.pptView.frame), ButtonViewHeight);
    
    //修改btnView的frame
    
    //修改btnView里面的btn的frame（根据btnView的大小决定）
    //        BOOL lessThanBtnViewWidth = NO;
    //        CGFloat btnWidth = ButtonWidth;
    //        if (CGRectGetWidth(self.buttonView.frame) < (ButtonWidth * self.tableViewNum)) {
    //            lessThanBtnViewWidth = YES;
    //            btnWidth = CGRectGetWidth(self.buttonView.frame) / self.tableViewNum;
    //        }
    //        for (int i = 0; i < self.tableViewNum; i ++) {
    //            UIButton * btn             = (UIButton *)[self.btnView viewWithTag:100 + i];
    //            CGRect frame               = btn.frame;
    //            frame.size.width           = btnWidth;
    //            frame.origin.x             = i * btnWidth;
    //            btn.frame                  = frame;
    //            UIImageView * rcircleImage = (UIImageView *)[self.btnView viewWithTag:200 + i];
    //            rcircleImage.frame         = CGRectMake(btnWidth - 7 - btnWidth/ButtonWidth*3, 7, 6, 6);
    //
    ////            CGRect selectViewFrame     = self.selectView.frame;
    ////            selectViewFrame.size.width = btnWidth;
    ////            self.selectView.frame      = selectViewFrame;
    //        }
    
    //修改scrollView的frame和contentSize
    self.scrollView.frame = portrait?CGRectMake(0, CGRectGetMaxY(self.pptView.frame) + ButtonViewHeight, ScreenSize.width, ScreenSize.height - CGRectGetMaxY(self.pptView.frame) - ButtonViewHeight):CGRectMake(CGRectGetMaxX(self.pptView.frame), CGRectGetMaxY(self.buttonView.frame), self.view.bounds.size.width - CGRectGetMaxX(self.pptView.frame), self.view.bounds.size.height - CGRectGetMaxY(self.buttonView.frame));
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.scrollView.frame) * self.tableViewNum, CGRectGetHeight(self.scrollView.frame));
    self.scrollView.contentOffset = CGPointMake(0, 0);
    [self.buttonView selectButton:self.buttonView.chatBtn];
    
    //修改tableView的frame和刷新tableView
    for (int i = 0; i < self.tableViewNum; i ++) {
        UIView * tableView = (UIView *)[self.scrollView viewWithTag:300 + i];
        tableView.frame    = CGRectMake(i * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height - 50);
        if (i == 2) {
            tableView.frame = CGRectMake(i * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        }
        id tableViewV = tableView.nextResponder;
        RootTableViewController * tableViewVC = tableViewV;
        
        //        if (i == 0 || i == 1) {
        [tableViewVC recalculateCellHeight];
        //        }else
        [tableViewVC.tableView reloadData];
        
        
    }
    self.shadowView.frame = self.scrollView.frame;
    
    //修改chatTF和askTF的frame
    self.chatTFView.frame = CGRectMake(0, CGRectGetHeight(self.scrollView.frame) - 50, CGRectGetWidth(self.scrollView.frame), 50);
    
    if (!portrait && IsIPAD && !_longCoverTFView) {
        [self.view insertSubview:self.longCoverTFView belowSubview:self.cameraView];
    }
    _longCoverTFView.hidden = portrait;
    _longCoverTFView.frame = CGRectMake(CGRectGetMaxX(self.pptView.frame), CGRectGetMaxY(self.scrollView.frame)-CGRectGetHeight(self.chatTFView.frame), CGRectGetWidth(self.scrollView.frame), CGRectGetHeight(self.chatTFView.frame));
    self.askTFView.frame = CGRectMake(CGRectGetWidth(self.scrollView.frame), CGRectGetHeight(self.scrollView.frame) - 50, CGRectGetWidth(self.scrollView.frame), 50);
    
    //改变输入框下划线长度
    [self.askTFView askTFFrameChanged];
    
    //改变遮布
    self.coverView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    //抽奖投票
    self.voteVC.view.frame = self.view.bounds;
    self.voteEndVC.view.frame = self.view.bounds;
    self.lotteryVC.view.frame = self.view.bounds;
    self.myLotteryVC.view.frame = self.view.bounds;
    
    //网络选择
    self.networkSelectionVC.view.frame = self.view.bounds;
    self.networkSelectionVC.rotated    = !portrait;
    
    originPPTFrame = self.pptView.frame;
    
    if (!IsIPAD||!self.iPadAutoRotate) {
        self.buttonView.hidden = !portrait;
        self.scrollView.hidden = !portrait;
        self.shadowView.hidden = !portrait;
    }
    [self removeNetworkTipsView];
    [self removeReplyTipsView];
}

- (void)orientationLandscape
{
    @synchronized (self) {
        //先记住转之前的statusBar的方向
        self.orientation = APPLICATION.statusBarOrientation;
        
        //如果现在的statusBar是竖直方向就旋转
        if (APPLICATION.statusBarOrientation == UIInterfaceOrientationPortrait) {
            CGFloat duration = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
            [UIView animateWithDuration:duration animations:^{
                [APPLICATION setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
                [APPLICATION setStatusBarHidden:YES];
                
                self.view.transform = CGAffineTransformRotate(self.view.transform, M_PI_2);
            }];
        }
        self.view.bounds = CGRectMake(0, 0, ScreenSize.width, ScreenSize.height);
        
        //根据系统版本判断用哪种方式去screenSize
        double version = [[UIDevice currentDevice].systemVersion doubleValue];
        if (version < 8.0) {
            self.view.bounds = CGRectMake(0, 0, ScreenSize.height, ScreenSize.width);
        }
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
        
        [self orientationPortrait:NO];
    }
}

- (void)chatMessageCome{
    [self.buttonView showTipsInButton:self.buttonView.chatBtn];
}
- (void)askMessageCome{
    [self.buttonView showTipsInButton:self.buttonView.askBtn];
}
- (void)noticeMessageCome{
    [self.buttonView showTipsInButton:self.buttonView.noticeBtn];
}

#pragma mark - =========== 接口回调处理 ============
- (void)liveModeChange:(id)obj
{
    WeakSelf
    TalkfunLiveMode mode = [obj[@"currentMode"] intValue];
    TalkfunLiveMode mode2 = [obj[@"beforeMode"] intValue];
    if (mode != mode2) {
        weakSelf.tipsLabel.text = @"切换中...";
        [weakSelf.pptView addSubview:weakSelf.tipsLabel];
    }
}

- (void)voteNew:(id)obj
{
    WeakSelf
    [weakSelf.voteNewDict setObject:obj forKey:obj[@"vid"]];
    [APPLICATION sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    NSString * nickname  = obj[@"info"][@"nickname"];
    NSString * startTime = obj[@"info"][@"startTime"];
    NSArray * arr        = [startTime componentsSeparatedByString:@" "];
    NSString * message   = [NSString stringWithFormat:@"管理员 %@ 在%@发起了一个",nickname,arr[1]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"vote:new":message,@"nickname":nickname,@"vid":obj[@"vid"]}];
    
    [weakSelf.voteVC removeFromSuperview];
    //
    [weakSelf.voteVC refreshUIWithParams:obj];
    
    weakSelf.voteVC.view.alpha = 1.0;
    [weakSelf.view addSubview:weakSelf.voteVC.view];
}

- (void)votePub:(id)obj
{
    WeakSelf
    [weakSelf.voteNewDict removeObjectForKey:obj[@"info"][@"vid"]];
    [weakSelf.votePubDict setObject:obj[@"isShow"] forKey:obj[@"info"][@"vid"]];
    [APPLICATION sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    NSString * nickname = obj[@"info"][@"nickname"];
    NSString * endTime  = obj[@"info"][@"endTime"];
    NSArray * arr2      = [endTime componentsSeparatedByString:@" "];
    NSString * message  = [NSString stringWithFormat:@"管理员 %@ 在%@结束了投票。",nickname,arr2[1]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"vote:pub":message,@"nickname":nickname,@"vid":obj[@"info"][@"vid"],@"isShow":obj[@"isShow"]}];
    if ([obj[@"isShow"] integerValue] == 0) {
        return ;
    }
    [weakSelf.voteVC removeFromSuperview];
    
    [weakSelf.voteVC refreshUIWithResult:obj];
    weakSelf.voteVC.view.alpha = 1.0;
    [weakSelf.view addSubview:weakSelf.voteVC.view];
}

- (void)lotteryStart:(id)obj
{
    WeakSelf
    [weakSelf.myLotteryVC.view removeFromSuperview];
    [APPLICATION sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    weakSelf.lotteryVC.view.alpha = 1.0;
    [weakSelf.view addSubview:weakSelf.lotteryVC.view];
    [weakSelf.lotteryVC refreshUIWithInfo:nil];
}

- (void)lotteryStop:(id)obj
{
    WeakSelf
    [weakSelf.lotteryVC.view removeFromSuperview];
    [APPLICATION sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    NSArray * arr              = obj[@"result"];
    NSString * launch_nickname = arr[0][@"launch_nickname"];
    NSString * nickname        = arr[0][@"nickname"];
    NSString * message         = [NSString stringWithFormat:@"通知: %@ 发起了抽奖,恭喜 %@ 中奖!",launch_nickname,nickname];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"lottery:stop":message,@"launch_nickname":launch_nickname,@"nickname":nickname}];
    if (![weakSelf.view.subviews containsObject:weakSelf.lotteryVC.view]) {
        
        NSString * xid = [NSString stringWithFormat:@"%@",obj[@"result"][0][@"xid"]];
        NSString * myXid = [NSString stringWithFormat:@"%@",weakSelf.me[@"xid"]];
        if ([xid isEqualToString:myXid]) {
            
            weakSelf.myLotteryVC.view.alpha = 1.0;
            [weakSelf.view addSubview:weakSelf.myLotteryVC.view];
            [weakSelf.myLotteryVC refreshUIWithInfo:obj];
        }else
        {
            weakSelf.lotteryVC.view.alpha = 1.0;
            [weakSelf.view addSubview:weakSelf.lotteryVC.view];
            [weakSelf.lotteryVC refreshUIWithInfo:obj];
        }
    }
}

- (void)chatDisable:(id)obj
{
    WeakSelf
    NSString * nickname = obj[@"nickname"];
    if ([weakSelf.me[@"xid"] isEqualToNumber:obj[@"xid"]]) {
        return ;
    }
    else if ([weakSelf.me[@"role"] isEqualToString:TalkfunMemberRoleAdmin] || [weakSelf.me[@"role"] isEqualToString:TalkfunMemberRoleSpadmin])
    {
        NSString * message = [NSString stringWithFormat:@"系统:【%@】已经被管理员\"禁言\"",nickname];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"chat:disable":message,@"nickname":nickname}];
    }
}

- (void)chatDisableAll:(id)obj
{
    int status = [obj[@"status"] intValue];
    self.chatTFView.userInteractionEnabled = status==1?NO:YES;
    self.chatTFView.alpha = status==1?0.5:1;
    
    self.longTextfieldView.userInteractionEnabled = status==1?NO:YES;
    self.longTextfieldView.alpha = status==1?0.5:1;
    
    
    
    self.longCoverTFView.userInteractionEnabled = status==1?NO:YES;
    self.longCoverTFView.alpha = status==1?0.5:1;
    //禁言
    if(status==1){
        self.disableAll = YES;
        NSString * message = [NSString stringWithFormat:@"公共广播: %@",@"锁屏模式已开启"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"broadcast":message,@"mess":@"锁屏模式已开启"}];
        
        [self setWordsNotAllowed];
        
        
        //能说话
    }else if(status == 0){
        self.disableAll = NO;
        NSString * message = [NSString stringWithFormat:@"公共广播: %@",@"锁屏模式已关闭"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"broadcast":message,@"mess":@"锁屏模式已关闭"}];
        [self setWordsAllowed];
        
    }
}

- (void)flowerGetInit:(id)obj
{
    WeakSelf
    if ([obj[@"code"] integerValue] == 0 && ([obj[@"amount"] integerValue] > 0 && [obj[@"amount"] integerValue] <=3)) {
        //        NSString * amount = [NSString stringWithFormat:@"%@",obj[@"amount"]];
        [weakSelf.chatTFView flower:YES];
        [_longCoverTFView flower:YES];
    }
    else if ([obj[@"code"] isEqualToNumber:@(1202)]){
        //        NSString * tipsString = @" 还没开始上课哟~  ";
    }
    else{
        [weakSelf.chatTFView flower:NO];
        [_longCoverTFView flower:NO];
    }
}

- (void)flowerTotal:(id)obj
{
    WeakSelf
    //    NSString * numString = [NSString stringWithFormat:@"%@",obj[@"total"]];
    NSInteger total      = [obj[@"total"] integerValue];
    if (total != 0) {
        [weakSelf.chatTFView flower:YES];
        [_longCoverTFView flower:YES];
    }
}

- (void)memberJoinOther:(id)obj{
    //    WeakSelf
    //    weakSelf.messageView.renshuLabel.text = [NSString stringWithFormat:@"%@人",obj[@"total"]];
}

- (void)memberLeave:(id)obj{
    //    WeakSelf
    //    weakSelf.messageView.renshuLabel.text = [NSString stringWithFormat:@"%@人",obj[@"total"]];
}

- (void)liveStart:(id)obj
{
    WeakSelf
    //    weakSelf.messageView.liveStatusLabel.text = obj[@"title"];
    if (weakSelf.unStart == YES && !weakSelf.isFirst) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clear" object:nil];
    }
    weakSelf.unStart = NO;
    //    [weakSelf.functionView liveStart:YES];
    
    [[TalkfunDownloadManager shareManager] pauseAllDownload];
}

- (void)liveStop:(id)obj
{
    WeakSelf;
    //    weakSelf.messageView.liveStatusLabel.text = @"直播已结束";
    weakSelf.unStart = YES;
    weakSelf.isFirst = NO;
    
    //    [weakSelf.functionView liveStart:NO];
    
    weakSelf.networkSelectionVC.selectedNumber = 0;
    if (weakSelf.isExchanged) {
        [weakSelf.talkfunSDK exchangePPTAndCameraContainer];
    }
    
    //    [weakSelf.networkStatusView networkStatusViewHide:YES];
}

- (void)memberKick:(id)obj
{
    WeakSelf
    NSString * nickname = obj[@"nickname"];
    NSString * message  = [NSString stringWithFormat:@"通知: 管理员把 %@ 请出了房间",nickname];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"broadcastInfo" object:nil userInfo:@{@"member:kick":message,@"nickname":nickname}];
    if ([weakSelf.me[@"xid"] isEqualToNumber:obj[@"xid"]]) {
        PERFORM_IN_MAIN_QUEUE([weakSelf removeOnSomething];
                              [weakSelf.kickoutAlertView show];
                              )
    }
}

- (void)memberForceout:(id)obj
{
    WeakSelf
    PERFORM_IN_MAIN_QUEUE([weakSelf removeOnSomething];
                          [weakSelf.forceoutAlertView show];
                          )
}

#pragma mark 锁屏模式,暂时不能发言哦
- (void)setWordsNotAllowed{
    
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
        self.chatTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"当前是锁屏模式,暂时不能发言哦..." attributes:@{NSForegroundColorAttributeName: LIGHTBLUECOLOR}];
        self.longTextfieldView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"当前是锁屏模式,暂时不能发言哦..." attributes:@{NSForegroundColorAttributeName: LIGHTBLUECOLOR}];
        self.longCoverTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"当前是锁屏模式,暂时不能发言哦..." attributes:@{NSForegroundColorAttributeName: LIGHTBLUECOLOR}];
    }else{
        
        
        
        self.chatTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"当前是锁屏模式,暂时不能发言哦..." attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        self.longTextfieldView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"当前是锁屏模式,暂时不能发言哦..." attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        self.longCoverTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"当前是锁屏模式,暂时不能发言哦..." attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        
    }
    
    
    
}
#pragma mark 我要聊天
- (void)setWordsAllowed
{
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
        
        self.chatTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"我要聊天..." attributes:@{NSForegroundColorAttributeName: LIGHTBLUECOLOR}];
        self.longTextfieldView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"我要聊天..." attributes:@{NSForegroundColorAttributeName: LIGHTBLUECOLOR}];
        self.longCoverTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"我要聊天..." attributes:@{NSForegroundColorAttributeName: LIGHTBLUECOLOR}];
    }else{
        
        self.chatTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"我要聊天..." attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        self.longTextfieldView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"我要聊天..." attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        self.longCoverTFView.tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"我要聊天..." attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    }
}
- (void)roomInit:(id)obj
{
    WeakSelf
    //    [weakSelf.messageView roomInit:obj];
    weakSelf.me = [[NSDictionary alloc] initWithDictionary:obj[@"roomInfo"][@"me"]];
    //MARK:滚动条
    [weakSelf.pptView addSubview:weakSelf.hornView];
    [weakSelf.hornView announceRoll:obj[@"roomInfo"][@"announce"][@"roll"]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notice" object:nil userInfo:@{@"mess":obj[@"roomInfo"][@"announce"][@"notice"]}];
    
    int stat =[obj[@"roomInfo"][@"roomdata"][@"room"][@"chat"][@"disableall"]intValue];
    
    
    //禁
    if (stat==1) {
        NSLog(@"禁文");
        self.disableAll = YES;
        [self setWordsNotAllowed];
        
        self.chatTFView.userInteractionEnabled = NO;
        self.longTextfieldView.userInteractionEnabled = NO;
        self.longCoverTFView.userInteractionEnabled = NO;
        
        self.longTextfieldView.alpha = 0.5;
        self.chatTFView.alpha = 0.5;
        //         self.longCoverTFView.alpha = 0.5;
    }else if (stat ==0){
        self.disableAll = NO;
        NSLog(@"能说话");
        [self setWordsAllowed];
        
        
        self.longTextfieldView.userInteractionEnabled = YES;
        self.chatTFView.userInteractionEnabled = YES;
        self.longCoverTFView.userInteractionEnabled = NO;
        
        
        self.longTextfieldView.alpha = 1;
        self.chatTFView.alpha = 1;
        self.longCoverTFView.alpha = 1;
    }
}

- (void)memberTotal:(id)obj
{
    //    WeakSelf
    //    if ([obj isKindOfClass:[NSDictionary class]] && obj[@"total"]) {
    //        weakSelf.messageView.renshuLabel.text = [NSString stringWithFormat:@"%@人",obj[@"total"]];
    //    }
}

- (void)networkSpeed:(id)obj
{
    WeakSelf
    if (weakSelf.unStart) {
        return ;
    }
    [self.networkSelectionVC networkSpeed:obj];
    
    TalkfunNetworkStatus networkStatus = [obj[@"networkStatus"] intValue];
    
    if (networkStatus == TalkfunNetworkStatusBad && networkStatus != self.networkStatus) {
        [weakSelf addNetworkTipsView];
    }
    weakSelf.networkStatus = networkStatus;
}
#pragma mark - =========== 接口回调处理结束 ============

#pragma mark - 加手势
- (void)addGesture
{
    //scrollview加手势
    UITapGestureRecognizer * tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGR:)];
    [self.scrollView addGestureRecognizer:tapGR];
    
    //ppt加手势
    UITapGestureRecognizer * pptTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pptTapGR:)];
    [self.pptView addGestureRecognizer:pptTapGR];
    
    UITapGestureRecognizer * pptShareTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pptShareTapGR:)];
    pptShareTapGR.numberOfTapsRequired = 2;
    [self.pptView addGestureRecognizer:pptShareTapGR];
    
    //摄像头加手势
    UIPanGestureRecognizer *panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(objectDidDragged:)];
    panGR.maximumNumberOfTouches = 2;
    [self.cameraView addGestureRecognizer:panGR];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.cameraView addGestureRecognizer:tap];
    
    originPPTFrame = self.pptView.frame;
    //摄像头缩放
    //    UIPinchGestureRecognizer * pinchGR = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGR:)];
    //    [self.cameraView addGestureRecognizer:pinchGR];
    
    UITapGestureRecognizer * inputBackgroundTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inputBackgroundTapGR:)];
    [self.inputBackgroundView addGestureRecognizer:inputBackgroundTapGR];
}

- (void)inputBackgroundTapGR:(UITapGestureRecognizer *)tapGR{
    
}
#pragma mark - 手势的方法
- (void)tap:(UITapGestureRecognizer *)tap
{
    //收键盘
    [self.view endEditing:YES];
    [self expressionsViewShow:NO];
    //    self.expressionsView.hidden = YES;
    //    self.expressionsView2.hidden = YES;
    static CGAffineTransform transform;
    static CGRect frame;
    if (self.cameraView.frame.size.height != ScreenSize.height) {
        transform = self.cameraView.transform;
        //先保存原来cameraView的frame的大小
        frame = self.cameraView.frame;
        //设为黑色和全屏
        self.cameraView.backgroundColor = [UIColor blackColor];
        [UIView animateWithDuration:0.5 animations:^{
            self.cameraView.transform = CGAffineTransformIdentity;
            self.cameraView.frame = CGRectMake(0, 0, ScreenSize.width, ScreenSize.height);
        }];
    }else
    {
        [UIView animateWithDuration:0.5 animations:^{
            self.cameraView.frame = frame;
        } completion:^(BOOL finished) {
            //设为原来的颜色和原来的frame
            self.cameraView.backgroundColor = [UIColor blackColor];
        }];
    }
}

- (void)tapGR:(UITapGestureRecognizer *)tapGR{
    //收键盘
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [self expressionsViewShow:NO];
}

- (void)pptTapGR:(UITapGestureRecognizer *)pptTapGR
{
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight && (CGRectGetMinY(self.longTextfieldView.frame)+50<CGRectGetMaxY(self.pptView.frame))) {
        return;
    }
    self.pptsFunctionView.hidden = !self.pptsFunctionView.hidden;
}

static CGRect originPPTFrame;
- (void)pptShareTapGR:(UITapGestureRecognizer *)pptShareTapGR
{
    if (pptShareTapGR.state == UIGestureRecognizerStateEnded) {
        CGPoint location = [pptShareTapGR locationInView:self.pptView];
        if (!self.pptsFunctionView.hidden && (location.y<50||location.y>CGRectGetHeight(self.pptView.frame)-50)) {
            return;
        }
    }
    if (!self.pptsFunctionView.fullScreenBtn.selected && [UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait) {
        [self manualFullScreen:YES];
    }else if (self.pptsFunctionView.fullScreenBtn.selected == YES && fromLandscape == YES){
        [self manualFullScreen:NO];
    }else{
        self.iPadAutoRotate = NO;
        [self fullScreen];
    }
}

- (void)pinchGR:(UIPinchGestureRecognizer *)pinchGR
{
    if (self.cameraView.backgroundColor == [UIColor blackColor]) {
        return;
    }
    
    CGFloat scale = pinchGR.scale;
    
    static CGFloat lastScale = 1;
    
    if (pinchGR.state == UIGestureRecognizerStateBegan) {
        lastScale = 1;
    }
    
    CGFloat cameraViewX = self.cameraView.frame.origin.x;
    CGFloat cameraViewY = self.cameraView.frame.origin.y;
    CGFloat cameraViewW = self.cameraView.frame.size.width;
    CGFloat cameraViewH = self.cameraView.frame.size.height;
    
    if (scale > 1 && (cameraViewX < 0 || (cameraViewX + cameraViewW) > self.view.bounds.size.width || cameraViewY < 0 || (cameraViewY + cameraViewH) > self.view.bounds.size.height)) {
        return;
    }
    
    self.cameraView.transform = CGAffineTransformScale(self.cameraView.transform, scale / lastScale , scale / lastScale);
    
    lastScale = scale;
    
}

- (void)objectDidDragged:(UIPanGestureRecognizer *)sender
{
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && self.pptsFunctionView.fullScreenBtn.selected == NO) {
        return;
    }
    if (sender.state == UIGestureRecognizerStateChanged) {
        
        //注意，这里取得的参照坐标系是该对象的上层View的坐标。
        CGPoint offset        = [sender translationInView:self.cameraView];
        UIView * draggableObj = self.cameraView;
        
        CGFloat finalX        = draggableObj.center.x + offset.x;
        CGFloat finalY        = draggableObj.center.y + offset.y;
        
        CGFloat minX          = self.cameraView.frame.size.width / 2;
        CGFloat maxX          = self.view.bounds.size.width - (self.cameraView.frame.size.width / 2);
        CGFloat minY          = self.cameraView.frame.size.height / 2;
        CGFloat maxY          = self.view.bounds.size.height - (self.cameraView.frame.size.height / 2);
        
        if(finalX < minX){
            finalX = minX;
        }else if(finalX > maxX){
            finalX = maxX;
        }
        
        if(finalY < minY){
            finalY = minY;
        }else if(finalY > maxY){
            finalY = maxY;
        }
        
        //通过计算偏移量来设定draggableObj的新坐标
        draggableObj.center = CGPointMake(finalX, finalY);
        //初始化sender中的坐标位置。如果不初始化，移动坐标会一直积累起来。
        [sender setTranslation:CGPointMake(0, 0) inView:self.cameraView];
        
        NSString * model = [UIDevice currentDevice].model;
        if ([model isEqualToString:@"iPod touch"]) {
            //有图像的留存问题 重新渲染
            for (UIView * subView in self.view.subviews) {
                [subView setNeedsDisplayInRect:self.view.bounds];
                [subView setNeedsLayout];
                [subView setNeedsDisplay];
            }
        }else
        {
            //有图像的留存问题 重新渲染
            for (UIView * view in self.view.subviews) {
                for (UIView * subView in view.subviews) {
                    [subView setNeedsDisplayInRect:view.bounds];
                    [subView setNeedsLayout];
                    [subView setNeedsDisplay];
                }
                [view setNeedsDisplayInRect:self.view.bounds];
                [view setNeedsLayout];
                [view setNeedsDisplay];
            }
        }
    }
}


- (void)removeOnSomething
{
    WeakSelf
    [[NSNotificationCenter defaultCenter] removeObserver:weakSelf];
    [weakSelf.talkfunSDK.view removeFromSuperview];
    weakSelf.coverView.hidden = NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [self expressionsViewShow:NO];
    //    self.expressionsView.hidden = YES;
    //    self.expressionsView2.hidden = YES;
}

#pragma mark - scrollView 代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //    self.longTFView.hidden = YES;
    if (self.scrollView.contentOffset.x == 0) {
        if (APPLICATION.statusBarOrientation != UIInterfaceOrientationPortrait && self.pptsFunctionView.fullScreenBtn.selected==NO && IsIPAD) {
            _longCoverTFView.hidden = NO;
        }
    }else{
        _longCoverTFView.hidden = YES;
    }
    if (!IsIPAD && APPLICATION.statusBarOrientation != UIInterfaceOrientationPortrait) {
        return;
    }
    if (self.scrollView == scrollView) {
        NSInteger num = round((scrollView.contentOffset.x) / self.scrollView.frame.size.width);
        [self.view endEditing:YES];
        [self expressionsViewShow:NO];
        TalkfunNewButtonViewButton * btn = [self.buttonView viewWithTag:500+num];
        [self.buttonView selectButton:btn];
        if (scrollView.contentOffset.x > self.scrollView.frame.size.width * (self.tableViewNum - 1)) {
            self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width * (self.tableViewNum - 1), 0) ;
        }
        if (CGRectGetWidth(self.scrollView.frame)) {
            self.buttonView.selectViewLeadingSpace.constant = scrollView.contentOffset.x * (CGRectGetWidth(btn.frame) / CGRectGetWidth(self.scrollView.frame));
        }
    }
}

#pragma mark - 监听键盘
- (void)keyBoardDidShow:(NSNotification *)notification
{
    NSDictionary * keyboardInfo = [notification userInfo];
    NSValue * keyboardFrameEnd  = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
    
    [self keyBoardDidHide:nil];
    WeakSelf
    [UIView animateWithDuration:0.1 animations:^{
        if (weakSelf.longTextfieldView.tf.isFirstResponder) {
            weakSelf.longTextfieldView.frame = CGRectMake(0, CGRectGetHeight(weakSelf.pptView.frame)-50, CGRectGetWidth(weakSelf.pptView.frame), 50);
            weakSelf.longTextfieldView.transform = CGAffineTransformMakeTranslation(0, - keyboardFrameEndRect.size.height+UpSpace);
        }else if (weakSelf.chatTFView.tf.isFirstResponder || weakSelf.askTFView.myTextField.isFirstResponder || _longCoverTFView.tf.isFirstResponder){
            weakSelf.chatTFView.transform = CGAffineTransformMakeTranslation(0, - keyboardFrameEndRect.size.height+UpSpace);
            weakSelf.askTFView.transform = CGAffineTransformMakeTranslation(0, - keyboardFrameEndRect.size.height+UpSpace);
            _longCoverTFView.transform = CGAffineTransformMakeTranslation(0, - keyboardFrameEndRect.size.height);
        }
        NSArray * subViews = weakSelf.view.subviews;
        for (UIView * subView in subViews) {
            double version    = [[UIDevice currentDevice].systemVersion doubleValue];
            if (version < 8.0 && APPLICATION.statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
                weakSelf.chatTFView.transform = CGAffineTransformMakeTranslation(0, -keyboardFrameEndRect.size.width);
                _longCoverTFView.transform = CGAffineTransformMakeTranslation(0, -keyboardFrameEndRect.size.width);
                subView.transform = CGAffineTransformMakeTranslation(0, -UpSpace);
            }
            if (subView == weakSelf.chatTFView || subView == weakSelf.longTextfieldView) {
                subView.transform = CGAffineTransformMakeTranslation(0, - keyboardFrameEndRect.size.height+UpSpace);
                continue;
            }else if (subView == _longCoverTFView){
                continue;
            }else if (subView == weakSelf.cameraView){
                subView.transform = CGAffineTransformMakeTranslation(0, (weakSelf.view.bounds.size.height- CGRectGetHeight(keyboardFrameEndRect)-50<CGRectGetMaxY(weakSelf.cameraView.frame))?-(CGRectGetMaxY(weakSelf.cameraView.frame)-weakSelf.view.bounds.size.height+ CGRectGetHeight(keyboardFrameEndRect)+50):0);
            }else{
                subView.transform = CGAffineTransformMakeTranslation(0, -UpSpace);
            }
        }
    }];
}
- (void)keyBoardDidHide:(NSNotification *)notification
{
    WeakSelf
    [UIView animateWithDuration:0.1 animations:^{
        weakSelf.chatTFView.transform = CGAffineTransformIdentity;
        weakSelf.longTextfieldView.transform = CGAffineTransformIdentity;
        weakSelf.longTextfieldView.frame = CGRectMake(0, CGRectGetHeight(weakSelf.pptView.frame)-50, CGRectGetWidth(weakSelf.pptView.frame)/2.0, 50);
        weakSelf.askTFView.transform = CGAffineTransformIdentity;
        NSArray * subViews = weakSelf.view.subviews;
        for (UIView * subView in subViews) {
            subView.transform = CGAffineTransformIdentity;
        }
    }];
}
//longTF与chatTF切换
- (void)infoAction:(NSNotification *)notification
{
    id obj = notification.object;
    if (obj != self.chatTFView.tf && obj != self.longTextfieldView.tf && obj != _longCoverTFView.tf) {
        return;
    }
    if (self.chatTFView.tf == obj) {
        if (self.chatTFView.tf.text.length != 0) {
            [self.chatTFView showSendButton:YES];
        }else{
            [self.chatTFView showSendButton:NO];
        }
        self.longTextfieldView.tf.text = self.chatTFView.tf.text;
        _longCoverTFView.tf.text = self.chatTFView.tf.text;
    }else if (_longCoverTFView.tf == obj){
        if (_longCoverTFView.tf.text.length != 0) {
            [_longCoverTFView showSendButton:YES];
            [self.chatTFView showSendButton:YES];
        }else{
            [_longCoverTFView showSendButton:NO];
            [self.chatTFView showSendButton:NO];
        }
        self.chatTFView.tf.text = _longCoverTFView.tf.text;
        self.longTextfieldView.tf.text = _longCoverTFView.tf.text;
    }else{
        self.chatTFView.tf.text = self.longTextfieldView.tf.text;
        _longCoverTFView.tf.text = self.chatTFView.tf.text;
    }
}

#pragma mark - 旋转
- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    if (APPLICATION.statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
        return UIInterfaceOrientationMaskLandscapeRight;
    }
    if (APPLICATION.statusBarOrientation == UIInterfaceOrientationPortrait) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskPortrait;
}

//加载数据
- (void)loadData:(NSString *)data
{
    if (self.pptsFunctionView.danmuBtn.selected) {
        return;
    }
    NSMutableArray * descriptors = [[NSMutableArray alloc]init];
    [descriptors addObject:[self walkTextSpriteDescriptorWithDelay:0*2+1 chat:data]];
    [_barrageRender load:descriptors];
}
//MARK:弹幕描述符生产方法
- (BarrageDescriptor *)walkTextSpriteDescriptorWithDelay:(NSTimeInterval)delay chat:(NSString *)data{
    BarrageDescriptor * descriptor = [[BarrageDescriptor alloc]init];
    descriptor.spriteName = NSStringFromClass([BarrageWalkImageTextSprite class]);
    descriptor.params[@"text"] = data;
    ColorArray
    UIColor *tempColor = TempColor;
    descriptor.params[@"textColor"] = tempColor;
    descriptor.params[@"speed"] = @(100 * (double)random()/RAND_MAX+50);
    descriptor.params[@"direction"] = @(1);
    descriptor.params[@"delay"] = @(delay);
    descriptor.params[@"side"] = @(BarrageFloatSideCenter);
    descriptor.params[@"mandatorySize"] = @(111);
    return descriptor;
}
#pragma mark - BarrageRendererDelegate
- (NSTimeInterval)timeForBarrageRenderer:(BarrageRenderer *)renderer{
    NSTimeInterval interval = [[NSDate date]timeIntervalSinceDate:_startTime];
    return interval;
}

#pragma mark - alertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //退出
    if (alertView == _quitAlertView) {
        if (buttonIndex == 1) {
            [self quit];
        }
    }else if (alertView == _forceoutAlertView){
        if (buttonIndex == 1) {
            [self refresh];
        }else if (buttonIndex == 0){
            [self quit];
        }
    }else if (alertView == _kickoutAlertView){
        if (buttonIndex == 0) {
            [self quit];
        }
    }
}
- (void)quit{
    [self.talkfunSDK destroy];
    QUITCONTROLLER(self)
}
- (void)refresh{
    if (APPLICATION.statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
        [self orientationPortrait];
    }
    [self.talkfunSDK destroy];
    self.talkfunSDK = nil;
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.longTextfieldView = nil;
    self.pptsFunctionView = nil;
    self.chatTFView = nil;
    self.expressionViewVC = nil;
    [self.barrageRender stop];
    self.barrageRender = nil;
    self.isExchanged = NO;
    for (UIViewController * vc in self.childViewControllers) {
        [vc willMoveToParentViewController:nil];
        [vc removeFromParentViewController];
    }
    [self viewDidLoad];
    
    [self viewWillAppear:YES];
    [self viewDidAppear:YES];
    self.coverView.hidden = YES;
}

#pragma mark - textfield delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.chatTFView.tf) {
        [self expressionsViewShow:NO];
        [self.chatTFView expressionBtnSelected:NO];
    }else if (textField == self.longTextfieldView.tf){
        [self expressionsViewShow:NO];
        self.inputBackgroundView.hidden = NO;
        [self.longTextfieldView expressionBtnSelected:NO];
    }else if (textField == _longCoverTFView.tf){
        [self expressionsViewShow:NO];
        [_longCoverTFView expressionBtnSelected:NO];
        CGRect frame = _longCoverTFView.frame;
        frame.origin.x = 0;
        frame.size.width = ScreenSize.width;
        _longCoverTFView.frame = frame;
        //        _longCoverTFView.frame = CGRectMake(0, CGRectGetMaxY(self.scrollView.frame)-CGRectGetHeight(self.chatTFView.frame), ScreenSize.width, CGRectGetHeight(self.chatTFView.frame));
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField == self.longTextfieldView.tf) {
        self.inputBackgroundView.hidden = YES;
    }
    else if (textField == _longCoverTFView.tf){
        CGRect frame = _longCoverTFView.frame;
        frame.origin.x = CGRectGetMaxX(self.pptView.frame);
        frame.size.width = CGRectGetWidth(self.scrollView.frame);
        _longCoverTFView.frame = frame;
        //        _longCoverTFView.frame = CGRectMake(CGRectGetMaxX(self.pptView.frame), CGRectGetMaxY(self.scrollView.frame)-CGRectGetHeight(self.chatTFView.frame), CGRectGetWidth(self.scrollView.frame), CGRectGetHeight(self.chatTFView.frame));
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length == 0) {
        NSDictionary * textMessage = GetTextMessage(textField.text);
        BOOL match = [textMessage[@"match"] boolValue];
        if (match) {
            NSRange range = NSRangeFromString(textMessage[@"range"]);
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            return NO;
        }
    }
    return YES;
}

- (NSInteger)getBarrageNum{
    NSInteger num = [_barrageRender spritesNumberWithName:NSStringFromClass([BarrageWalkImageTextSprite class])];
    NSLog(@"numnumnum::++++======:%ld",(long)num);
    return num;
}

- (void)dealloc{
    [_barrageRender stop];
}

#pragma mark - 懒加载
//MARK:scrollView及其上面的东西
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initScrollViewWithTarget:self frame:CGRectMake(0, CGRectGetMaxY(self.pptView.frame) + ButtonViewHeight, ScreenSize.width, ScreenSize.height - CGRectGetMaxY(self.pptView.frame) - ButtonViewHeight)];
        _scrollView.delegate = self;
        _scrollView.backgroundColor = DARKBLUECOLOR;
    }
    return _scrollView;
}
- (UIView *)shadowView{
    if (!_shadowView) {
        _shadowView = [[UIView alloc] initShadowViewWithFrame:self.scrollView.frame];
    }
    return _shadowView;
}
- (TalkfunButtonView *)buttonView{
    if (!_buttonView) {
        _buttonView = [TalkfunButtonView initView];
        [_buttonView buttonsAddTarget:self action:@selector(btnViewButtonsClicked:)];
        _buttonView.frame = CGRectMake(0, CGRectGetMaxY(self.pptView.frame), ScreenSize.width, ButtonViewHeight);
        [_buttonView selectButton:_buttonView.chatBtn];
    }
    return _buttonView;
}
//- (UIButton *)barrageButton{
//    if (!_barrageButton) {
//        _barrageButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _barrageButton.frame = CGRectMake(CGRectGetWidth(self.btnView.frame)-42.5, (CGRectGetHeight(self.btnView.frame)-23*38/45)/2, 38, 23*38/45);
//        [_barrageButton setImage:[UIImage imageNamed:@"openBullet"] forState:UIControlStateNormal];
//        [_barrageButton setImage:[UIImage imageNamed:@"closeBullet"] forState:UIControlStateSelected];
//        [_barrageButton addTarget:self action:@selector(barrageButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _barrageButton;
//}
- (TalkfunNewTextfieldView *)chatTFView{
    if (!_chatTFView) {
        _chatTFView = [TalkfunNewTextfieldView initView];
        _chatTFView.frame = CGRectMake(0, CGRectGetHeight(self.scrollView.frame)-50, CGRectGetWidth(self.scrollView.frame), 50);
        [_chatTFView createChatTFView:self action:@selector(chatButtonClicked:)];
        [_chatTFView.expressionButton addTarget:self action:@selector(expressionsBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        _chatTFView.userInteractionEnabled = NO;
    }
    return _chatTFView;
}
- (TalkfunTextfieldView *)askTFView{
    if (!_askTFView) {
        _askTFView = [[NSBundle mainBundle] loadNibNamed:@"TalkfunTextfieldView" owner:nil options:nil][0];
        _askTFView.frame = CGRectMake(ScreenSize.width, CGRectGetHeight(self.scrollView.frame) - 50, CGRectGetWidth(self.scrollView.frame), 50);
        [_askTFView createAskTFView:self action:@selector(askButtonClicked:)];
        _askTFView.myTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"我要提问..." attributes:@{NSForegroundColorAttributeName: LIGHTBLUECOLOR}];
    }
    return _askTFView;
}
- (TalkfunNewTextfieldView *)longCoverTFView{
    if (!_longCoverTFView && IsIPAD) {
        _longCoverTFView = [TalkfunNewTextfieldView initView];
        _longCoverTFView.tag = 866;
        [_longCoverTFView createChatTFView:self action:@selector(chatButtonClicked:)];
        [_longCoverTFView.expressionButton addTarget:self action:@selector(expressionsBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        _longCoverTFView.tf.text = self.chatTFView.tf.text;
        [_longCoverTFView flower:self.chatTFView.hasFlower];
    }
    return _longCoverTFView;
}
- (TalkfunExpressionViewController *)expressionViewVC{
    if (!_expressionViewVC) {
        WeakSelf
        _expressionViewVC = [[TalkfunExpressionViewController alloc] init];
        __weak typeof(_longCoverTFView) weakLongCoverTFView = _longCoverTFView;
        _expressionViewVC.expressionBlock = ^(NSString * expressionName){
            if ([expressionName isEqualToString:@"delete"]) {
                if (weakSelf.longTextfieldView.tf.text.length != 0) {
                    NSDictionary * textMessage = GetTextMessage(weakSelf.longTextfieldView.tf.text);
                    NSRange range = NSRangeFromString(textMessage[@"range"]);
                    weakSelf.longTextfieldView.tf.text = [weakSelf.longTextfieldView.tf.text stringByReplacingCharactersInRange:range withString:@""];
                    weakSelf.chatTFView.tf.text = [weakSelf.chatTFView.tf.text stringByReplacingCharactersInRange:range withString:@""];
                    weakLongCoverTFView.tf.text = [weakLongCoverTFView.tf.text stringByReplacingCharactersInRange:range withString:@""];
                }
            }else{
                TalkfunNewTextfieldView * longView = [weakSelf.view viewWithTag:866];
                weakSelf.longTextfieldView.tf.text = [NSString stringWithFormat:@"%@[%@]",weakSelf.longTextfieldView.tf.text,expressionName];
                weakSelf.chatTFView.tf.text = weakSelf.longTextfieldView.tf.text;
                [weakSelf.chatTFView showSendButton:YES];
                if (longView) {
                    longView.tf.text = weakSelf.chatTFView.tf.text;
                    [longView showSendButton:YES];
                }
            }
        };
        //        _expressionViewVC.view.hidden = YES;
    }
    return _expressionViewVC;
}
//MARK:滚动通知的整条东西
- (TalkfunHornView *)hornView
{
    if (!_hornView) {
        _hornView = [[NSBundle mainBundle] loadNibNamed:@"TalkfunHornView" owner:nil options:nil][0];
        _hornView.frame = CGRectMake(0, CGRectGetMaxY(self.pptView.frame) - 25, CGRectGetWidth(self.pptView.frame), 25);
        _hornView.hidden = YES;
    }
    return _hornView;
}
//ppt信息及按钮
- (TalkfunNewFunctionView *)pptsFunctionView{
    if (!_pptsFunctionView) {
        _pptsFunctionView = [TalkfunNewFunctionView initView];
        _pptsFunctionView.frame = CGRectMake(0, 0, CGRectGetWidth(self.pptView.frame), CGRectGetHeight(self.pptView.frame));
        [_pptsFunctionView buttonsAddTarget:self action:@selector(pptsFunctionButtonClicked:)];
        _pptsFunctionView.hidden = YES;
        
        self.longTextfieldView.frame = CGRectMake(0, CGRectGetHeight(self.pptView.frame)-50, CGRectGetWidth(self.pptView.frame)/2.0, 50);
        [_pptsFunctionView addSubview:self.longTextfieldView];
    }
    return _pptsFunctionView;
}
- (TalkfunLongTextfieldView *)longTextfieldView{
    if (!_longTextfieldView) {
        _longTextfieldView = [TalkfunLongTextfieldView initView];
        [_longTextfieldView.sendBtn addTarget:self action:@selector(chatButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_longTextfieldView.expressionBtn addTarget:self action:@selector(expressionsBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        _longTextfieldView.tf.delegate = self;
        _longTextfieldView.hidden = YES;
    }
    return _longTextfieldView;
}
- (void)pptsFunctionButtonClicked:(UIButton *)btn{
    NSLog(@"____click %ld____",(long)btn.tag);
    //返回按钮
    if (btn.tag == 200) {
//        [self.quitAlertView show];
        
        HYAlertView *alertView = [[HYAlertView alloc] initWithTitle:@"提示" message:@"确定要退出吗" buttonTitles:@"取消", @"确定", nil];
        self.alertView = alertView;
        alertView.alertViewStyle = HYAlertViewStyleDefault;
        
        alertView.isOrientationLandscape = self.isOrientationLandscape;
        
        WeakSelf
        [alertView showInView:self.view completion:^(HYAlertView *alertView, NSInteger selectIndex) {
            NSLog(@"点击了%d", (int)selectIndex);
            if (selectIndex == 1) {
                [weakSelf quit];
            }
        }];
    }
    //隐藏camera按钮
    else if (btn.tag == 201){
        if (self.unStart || self.isDesktop) {
            return;
        }
        btn.selected = !btn.selected;
        if (btn.selected) {
            if (self.isExchanged) {
                [self.talkfunSDK exchangePPTAndCameraContainer];
            }
            self.cameraView.hidden = YES;
            [btn setImage:[UIImage imageNamed:@"关闭摄像头"] forState:UIControlStateNormal];
            //            [self.talkfunSDK hideCamera];
        }else{
            if (self.isExchanged) {
                [self.talkfunSDK exchangePPTAndCameraContainer];
            }
            self.cameraView.hidden = NO;
            [btn setImage:[UIImage imageNamed:@"开启摄像头"] forState:UIControlStateNormal];
            //            [self.talkfunSDK showCamera];
        }
        if (IsIPAD && [UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && !self.pptsFunctionView.fullScreenBtn.selected) {
            [self reloadScrollView:btn.selected];
        }
    }
    //切换ppt和camera
    else if (btn.tag == 202)
    {
        if (self.cameraView.hidden) {
            return;
        }
        [self.talkfunSDK exchangePPTAndCameraContainer];
        self.isExchanged = !self.isExchanged;
    }
    //刷新
    else if (btn.tag == 203){
        [self refresh];
    }
    //网络选择按钮
    else if (btn.tag == 204){
        if (self.unStart) {
            [self.view toast:@"直播未开始" position:ToastPosition];
            return;
        }
        WeakSelf
        [self.talkfunSDK getNetworkList:^(id result) {
            if ([result[@"code"] intValue] == 0) {
                //                weakSelf.networkSelectionVC.NetworkSelectionArray = result[@"data"];
                weakSelf.networkSelectionVC.NetworkSelectionDict = result;
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.view toast:result[@"msg"] position:ToastPosition];
                });
            }
        }];
        //TODO:网络选择控制器
        [self.view addSubview:self.networkSelectionVC.view];
    }
    //弹幕
    else if (btn.tag == 205){
        btn.selected = !btn.selected;
        if (!btn.selected) {
            [btn setImage:[UIImage imageNamed:@"开启弹幕"] forState:UIControlStateNormal];
            //            [_barrageRender start];
            _barrageRender.view.hidden = NO;
        }else{
            [btn setImage:[UIImage imageNamed:@"关闭弹幕"] forState:UIControlStateNormal];
            //            [_barrageRender stop];
            _barrageRender.view.hidden = YES;
        }
    }
    //全屏按钮
    else if (btn.tag == 206){
        //        self.expressionsView.hidden = YES;
        //        self.expressionsView2.hidden = YES;
        //        if ([DEVICEMODEL hasPrefix:@"iPad"]) {
        if (btn.selected == NO && [UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait) {
            [self manualFullScreen:YES];
        }else if (btn.selected == YES && fromLandscape == YES){
            [self manualFullScreen:NO];
        }else{
            self.iPadAutoRotate = NO;
            [self fullScreen];
        }
        //        }
    }
}
- (void)reloadScrollView:(BOOL)cameraHide{
    [UIView animateWithDuration:0.25 animations:^{
        CGRect buttonViewFrame = self.buttonView.frame;
        buttonViewFrame.origin.y = cameraHide?0:CGRectGetMaxY(self.cameraView.frame);
        self.buttonView.frame = buttonViewFrame;
        
        //修改scrollView的frame和contentSize
        self.scrollView.frame = CGRectMake(CGRectGetMaxX(self.pptView.frame), CGRectGetMaxY(self.buttonView.frame), self.view.bounds.size.width - CGRectGetMaxX(self.pptView.frame), self.view.bounds.size.height - CGRectGetMaxY(self.buttonView.frame));
        self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.scrollView.frame) * self.tableViewNum, CGRectGetHeight(self.scrollView.frame));
        //        self.scrollView.contentOffset = CGPointMake(0, 0);
        
        //修改tableView的frame和刷新tableView
        for (int i = 0; i < self.tableViewNum; i ++) {
            UIView * tableView = (UIView *)[self.scrollView viewWithTag:300 + i];
            tableView.frame    = CGRectMake(i * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height - 50);
            if (i == 2) {
                tableView.frame = CGRectMake(i * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
            }
            id tableViewV = tableView.nextResponder;
            RootTableViewController * tableViewVC = tableViewV;
            [tableViewVC recalculateCellHeight];
        }
        self.shadowView.frame = self.scrollView.frame;
        
        //修改chatTF和askTF的frame
        self.chatTFView.frame = CGRectMake(0, CGRectGetHeight(self.scrollView.frame) - 50, CGRectGetWidth(self.scrollView.frame), 50);
        self.askTFView.frame = CGRectMake(CGRectGetWidth(self.scrollView.frame), CGRectGetHeight(self.scrollView.frame) - 50, CGRectGetWidth(self.scrollView.frame), 50);
        
        //改变输入框下划线长度
        [self.askTFView askTFFrameChanged];
    }];
}
static BOOL fromLandscape = NO;
- (void)manualFullScreen:(BOOL)fullScreen{
    [self removeNetworkTipsView];
    [self removeReplyTipsView];
    fromLandscape = fullScreen;
    [UIView animateWithDuration:0.25 animations:^{
        self.buttonView.hidden = fullScreen;
        self.scrollView.hidden = fullScreen;
        self.shadowView.hidden = fullScreen;
        
        self.pptView.frame = fullScreen?CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height):CGRectMake(0, 0, self.view.bounds.size.width * 7 / 10, self.view.bounds.size.height);
        self.longTextfieldView.frame = CGRectMake(0, CGRectGetHeight(self.pptView.frame)-50, CGRectGetWidth(self.pptView.frame)/2.0, 50);
        self.inputBackgroundView.frame = self.view.bounds;
        self.longTextfieldView.hidden = !fullScreen;
        self.expressionViewVC.view.frame = CGRectMake(0, ScreenSize.height, ScreenSize.width,ExpressionViewHeight());
        
        //改变喇叭滚动条的frame
        self.hornView.frame = CGRectMake(0, CGRectGetMaxY(self.pptView.frame) - 25, CGRectGetWidth(self.pptView.frame), 25);
        
        //加动画
        [self.hornView rollLabelAddAnimation];
        
        //修改摄像头的frame
        self.cameraView.transform = CGAffineTransformIdentity;
        self.cameraView.frame = !fullScreen?CGRectMake(CGRectGetMaxX(self.pptView.frame), 0, CGRectGetWidth(self.view.bounds)-CGRectGetWidth(self.pptView.frame), (ScreenSize.width-CGRectGetWidth(self.pptView.frame))*3.0/4.0):CGRectMake(CGRectGetMaxX(self.pptView.frame) - CGRectGetHeight(self.view.bounds)*3.0/10.0, CGRectGetMaxY(self.pptView.frame)-50-(CGRectGetHeight(self.view.bounds)*3.0/10.0)*3.0/4.0, CGRectGetHeight(self.view.bounds)*3.0/10.0, (CGRectGetHeight(self.view.bounds)*3.0/10.0)*3.0/4.0);
        
        self.buttonView.frame = CGRectMake(CGRectGetMaxX(self.pptView.frame), self.cameraView.hidden?0:CGRectGetMaxY(self.cameraView.frame), ScreenSize.width-CGRectGetWidth(self.pptView.frame), ButtonViewHeight);
    }];
    self.pptsFunctionView.fullScreenBtn.selected = fullScreen;
    [self.pptsFunctionView.fullScreenBtn setImage:[UIImage imageNamed:fullScreen?@"退出全屏":@"全屏"] forState:UIControlStateNormal];
    CGRect frame = _longCoverTFView.frame;
    frame.origin.x = CGRectGetMaxX(self.pptView.frame);
    _longCoverTFView.frame = frame;
    _longCoverTFView.hidden = fullScreen;
    
    if(self.disableAll ==YES){
        [self setWordsNotAllowed];
    }else{
        [self setWordsAllowed];
    }
}
- (void)fullScreen{
    [self expressionsViewShow:NO];
    [self.view endEditing:YES];
    
    
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
        self.pptsFunctionView.fullScreenBtn.selected = YES;
        [self.pptsFunctionView.fullScreenBtn setImage:[UIImage imageNamed:@"退出全屏"] forState:UIControlStateNormal];
        [self orientationLandscape];
        
    }else{
        self.pptsFunctionView.fullScreenBtn.selected = NO;
        [self.pptsFunctionView.fullScreenBtn setImage:[UIImage imageNamed:@"全屏"] forState:UIControlStateNormal];
        [self orientationPortrait];
        
    }
    self.iPadAutoRotate = YES;
    
    if(self.disableAll ==YES){
        [self setWordsNotAllowed];
    }else{
        [self setWordsAllowed];
    }
}
- (UIAlertView *)quitAlertView{
    if (!_quitAlertView) {
        _quitAlertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定要退出吗" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    }
    return _quitAlertView;
}

//网络选择懒加载
- (NetworkSelectionViewController *)networkSelectionVC
{
    if (_networkSelectionVC == nil) {
        _networkSelectionVC = [[NetworkSelectionViewController alloc] init];
        _networkSelectionVC.view.frame = [UIScreen mainScreen].bounds;
        WeakSelf
        _networkSelectionVC.NetworkOperators = ^(NSDictionary *str){
            
            [weakSelf.talkfunSDK setNetwork:str[@"operatorID"] sourceName:str[@"sourceName"]  selectedSegmentIndex:[str[@"selectedSegmentIndex"] integerValue]];
        };
        
        _networkSelectionVC.LineBlock = ^(NSString *str){
            
            [weakSelf.talkfunSDK lineSwitching:str];
        };
        
    }
    return _networkSelectionVC;
}
//投票
- (NSMutableDictionary *)voteNewDict
{
    if (!_voteNewDict) {
        _voteNewDict =  [NSMutableDictionary new];
    }
    return _voteNewDict;
}
- (NSMutableDictionary *)votePubDict
{
    if (!_votePubDict) {
        _votePubDict = [NSMutableDictionary new];
    }
    return _votePubDict;
}
- (NSMutableArray *)voteFinishArray
{
    if (!_voteFinishArray) {
        _voteFinishArray = [NSMutableArray new];
    }
    return _voteFinishArray;
}
- (VoteViewController *)voteVC
{
    if (!_voteVC) {
        _voteVC = [VoteViewController new];
        _voteVC.view.frame = self.view.frame;
        WeakSelf
        _voteVC.voteBlock = ^(NSString *vid,NSArray *optionsArray){
            
            [weakSelf.voteVC.view removeFromSuperview];
            //已经投票的加入已投票数组
            [weakSelf.voteFinishArray addObject:vid];
            NSMutableString * arrString = [NSMutableString new];
            [arrString appendString:@"["];
            if (optionsArray.count == 1) {
                [arrString appendFormat:@"%@]",optionsArray[0]];
            }else
            {
                NSString * composeStr = [optionsArray componentsJoinedByString:@","];
                [arrString appendFormat:@"%@]",composeStr];
            }
            NSDictionary * params = @{@"vid":vid, @"options":arrString};
            [weakSelf.talkfunSDK emit:@"vote:post:data" parameter:params callback:^(id obj) {
                
                if ([obj[@"code"] isEqualToNumber:@(0)]) {
                    weakSelf.voteEndVC.voteTitle.text = @"投票成功";
                    weakSelf.voteEndVC.message = @"投票成功";
                }else
                {
                    weakSelf.voteEndVC.voteTitle.text = @"投票失败";
                    if ([obj[@"data"] isKindOfClass:[NSString class]]) {
                        weakSelf.voteEndVC.message = obj[@"data"];
                    }
                }
                weakSelf.voteEndVC.view.alpha = 1.0;
                [weakSelf.view addSubview:weakSelf.voteEndVC.view];
                [weakSelf.voteEndVC refreshUIWithAfterCommitted];
            }];
        };
    }
    return _voteVC;
}
- (VoteEndViewController *)voteEndVC
{
    if (!_voteEndVC) {
        _voteEndVC = [VoteEndViewController new];
        _voteEndVC.view.frame = self.view.frame;
    }
    return _voteEndVC;
}

//MARK:抽奖
- (LotteryViewController *)lotteryVC
{
    if (!_lotteryVC) {
        _lotteryVC = [LotteryViewController new];
        _lotteryVC.view.frame = self.view.frame;
    }
    return _lotteryVC;
}
- (MyLotteryViewController *)myLotteryVC
{
    if (!_myLotteryVC) {
        _myLotteryVC = [MyLotteryViewController new];
        _myLotteryVC.view.frame = self.view.frame;
    }
    return _myLotteryVC;
}
//MARK:遮布
- (UIView *)coverView
{
    if (!_coverView) {
        _coverView = [[UIView alloc] initWithFrame:self.view.frame];
        _coverView.backgroundColor = [UIColor whiteColor];
        PERFORM_IN_MAIN_QUEUE(_coverView.hidden = YES;
                              [self.view addSubview:_coverView];)
    }
    return _coverView;
}
//MARK:视频切换、暂停提示
- (UILabel *)tipsLabel
{
    if (!_tipsLabel) {
        _tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(self.pptsFunctionView.frame)+50, 100, 30)];
        _tipsLabel.textColor = [UIColor whiteColor];
        _tipsLabel.backgroundColor = [UIColor redColor];
        _tipsLabel.textAlignment = NSTextAlignmentCenter;
        _tipsLabel.font = [UIFont systemFontOfSize:14];
    }
    return _tipsLabel;
}

//被逼下线
- (UIAlertView *)forceoutAlertView{
    if (!_forceoutAlertView) {
        _forceoutAlertView = [[UIAlertView alloc] initWithTitle:@"你被迫下线了" message:nil delegate:self cancelButtonTitle:@"退出" otherButtonTitles:@"刷新", nil];
    }
    return _forceoutAlertView;
}
//被踢出房间
- (UIAlertView *)kickoutAlertView{
    if (!_kickoutAlertView) {
        _kickoutAlertView = [[UIAlertView alloc] initWithTitle:@"你被踢出房间了" message:nil delegate:self cancelButtonTitle:@"退出" otherButtonTitles:nil, nil];
    }
    return _kickoutAlertView;
}
//MARK:弹幕
- (BarrageRenderer *)barrageRender{
    if (!_barrageRender) {
        _barrageRender = [[BarrageRenderer alloc] init];
        _barrageRender.delegate = self;
        _barrageRender.redisplay = YES;
        [self.pptView addSubview:_barrageRender.view];
        [self.pptView bringSubviewToFront:_barrageRender.view];
    }
    return _barrageRender;
}
//MARK:网络较差提示
- (UIImageView *)networkTipsImageView{
    if (!_networkTipsImageView) {
        CGRect rect = [self.view convertRect:self.pptsFunctionView.networkBtn.frame toView:self.view];
        CGRect frame;
        UIImage * image;
        if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
            image = [UIImage imageNamed:@"网络提示1"];
            frame = CGRectMake(0, CGRectGetMaxY(rect), 210, 40);
        }else{
            image = [UIImage imageNamed:@"网络提示2"];
            frame = CGRectMake(0, CGRectGetMinY(rect)-40, 210, 40);
        }
        _networkTipsImageView = [[UIImageView alloc] initWithFrame:frame];
        _networkTipsImageView.center = CGPointMake(CGRectGetMidX(rect), _networkTipsImageView.center.y);
        _networkTipsImageView.contentMode = UIViewContentModeScaleAspectFit;
        _networkTipsImageView.image = image;
        UITapGestureRecognizer * tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(networkTipsTap:)];
        [_networkTipsImageView addGestureRecognizer:tapGR];
        _networkTipsImageView.userInteractionEnabled = YES;
    }
    return _networkTipsImageView;
}
- (void)networkTipsTap:(UITapGestureRecognizer *)tapGR{
    [UIView animateWithDuration:0.25 animations:^{
        self.networkTipsImageView.alpha = 0.0;
    }completion:^(BOOL finished) {
        [self removeNetworkTipsView];
    }];
}
- (void)removeNetworkTipsView{
    [_networkTipsImageView removeFromSuperview];
    _networkTipsImageView = nil;
}
- (void)removeReplyTipsView{
    [_replyTipsView removeFromSuperview];
    _replyTipsView = nil;
}

- (UIView *)inputBackgroundView{
    if (!_inputBackgroundView) {
        _inputBackgroundView = [[UIView alloc] initWithFrame:self.pptsFunctionView.frame];
        _inputBackgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        _inputBackgroundView.hidden = YES;
    }
    return _inputBackgroundView;
}

- (TalkfunReplyTipsView *)replyTipsView{
    if (!_replyTipsView) {
        _replyTipsView = [[NSBundle mainBundle] loadNibNamed:@"TalkfunReplyTipsView" owner:nil options:nil][0];
        CGRect frame = CGRectMake(0, CGRectGetHeight(self.view.bounds)-100, CGRectGetWidth(self.view.bounds), 50);
        if (IsIPAD && [UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && self.pptsFunctionView.fullScreenBtn.selected == NO) {
            frame = CGRectMake(CGRectGetMaxX(self.pptView.frame), CGRectGetHeight(self.view.bounds)-100, CGRectGetWidth(self.view.bounds)-CGRectGetWidth(self.pptView.frame), 50);
        }
        _replyTipsView.frame = frame;
        [_replyTipsView.tipsButton addTarget:self action:@selector(replyTipsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        WeakSelf
        _replyTipsView.closeBtnBlock = ^(){
            [weakSelf removeReplyTipsView];
        };
    }
    return _replyTipsView;
}
- (void)replyTipsButtonClicked:(UIButton *)button{
    [self btnViewButtonsClicked:self.buttonView.askBtn];
    [self removeReplyTipsView];
}

@end
