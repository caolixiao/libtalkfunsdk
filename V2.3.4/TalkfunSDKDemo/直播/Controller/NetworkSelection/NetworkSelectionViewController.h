//
//  NetworkSelectionViewController.h
//  Talkfun_demo
//
//  Created by moruiwei on 16/1/27.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import <UIKit/UIKit.h>
#define APPLICATION [UIApplication sharedApplication]
// name:block类型的别名
typedef void(^networkOperators)(NSDictionary *str);

typedef void(^lineBlock)(NSString *line);
@interface NetworkSelectionViewController : UIViewController
@property (nonatomic,assign ) BOOL           rotated;//屏幕方向
//@property (nonatomic,strong ) NSArray        * NetworkSelectionArray;//字典数据数组


@property (nonatomic,strong ) NSDictionary        * NetworkSelectionDict;//字典数据数组

@property (nonatomic, strong) NSMutableArray *choiceArray;//模型数据
@property (weak, nonatomic  ) IBOutlet UIButton       *exit;
@property (nonatomic, strong) networkOperators    NetworkOperators ;


@property (nonatomic, strong) lineBlock    LineBlock ;
//保存用户选择的网络
@property (assign, nonatomic) NSInteger      selectedNumber;

- (void)networkSpeed:(id)obj;

@end
