//
//  ChatTableViewController.m
//  Talkfun_demo
//
//  Created by talk－fun on 16/1/26.
//  Copyright © 2016年 talk-fun. All rights reserved.
//

#import "ChatTableViewController.h"
#import "ChatModel.h"
#import "MJExtension.h"
//#import "ChatCell.h"
#import "TalkfunNewChatCell.h"

@interface ChatTableViewController ()

@end

@implementation ChatTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(broadcastInfo:) name:@"broadcastInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chat:) name:@"chat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(flower:) name:@"flower" object:nil];
    
}

//送花接口被调用
- (void)flower:(NSNotification *)notification
{
    NSDictionary * params = notification.userInfo;
    [self.dataSource addObject:params[@"mess"]];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"chatMessageCome" object:nil];

//    NSInteger amount      = [params[@"mess"][@"amount"] integerValue];
//
//    NSMutableString * msg = [NSMutableString new];
//    [msg appendString:@"送给老师："];
//    for (int i = 0; i < amount; i ++) {
//        [msg appendString:@"[rose]"];
//    }
    
    //记住row的高度
//    CGRect rect = [msg boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 20, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
//    CGFloat height = rect.size.height + 30;
//    //        if (self.heightArray.count == 0) {
//    //            height += self.advanceHeight;
//    //        }
//    [self.heightArray addObject:@(height+10)];
    
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.dataSource.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

//抽奖 投票 踢人 广播 禁言
- (void)broadcastInfo:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chatMessageCome" object:nil];
    NSDictionary * messageDict = notification.userInfo;
    NSString * vote_new        = messageDict[@"vote:new"];
    NSString * vote_pub        = messageDict[@"vote:pub"];
    NSString * lottery_stop    = messageDict[@"lottery:stop"];
    NSString * broadcast       = messageDict[@"broadcast"];
    NSString * chat_disable    = messageDict[@"chat:disable"];
    NSString * member_kick     = messageDict[@"member:kick"];
    
    [self.dataSource addObject:messageDict];
    
    NSString * allString = nil;
    if (vote_new) {
        allString = vote_new;
    }
    else if (vote_pub)
    {
        allString = vote_pub;
    }
    else if (lottery_stop)
    {
        allString = lottery_stop;
    }
    else if (broadcast)
    {
        allString = broadcast;
    }
    else if (chat_disable)
    {
        allString = chat_disable;
    }
    else if (member_kick)
    {
        allString = member_kick;
    }
    
    
//    CGRect rect = [allString boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 40, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
    
//    if (rect.size.height < 40) {
//        rect.size.height = 40;
//    }
    
//    [self.heightArray addObject:@(rect.size.height+10)];
    
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.dataSource.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (NSString *)getStringWithModel:(ChatModel *)model{
    
    NSString * vote_new     = model.vote_new;
    NSString * vote_pub     = model.vote_pub;
    NSString * lottery_stop = model.lottery_stop;
    NSString * broadcast    = model.broadcast;
    NSString * chat_disable = model.chat_disable;
    NSString * member_kick  = model.member_kick;
    
    NSString * allString = nil;
    if (vote_new) {
        allString = vote_new;
        allString = [allString stringByAppendingString:@"投票"];
    }
    else if (vote_pub)
    {
        allString = vote_pub;
        if (![model.isShow isEqualToString:@"0"]) {
            allString = [allString stringByAppendingString:@"查看结果"];
        }
    }
    else if (lottery_stop)
    {
        allString = lottery_stop;
    }
    else if (broadcast)
    {
        allString = broadcast;
    }
    else if (chat_disable)
    {
        allString = chat_disable;
    }
    else if (member_kick)
    {
        allString = member_kick;
    }else{
        if (model.amount) {
            NSInteger amount = [model.amount integerValue];
            
            NSMutableString * msg = [NSMutableString new];
            [msg appendString:@"送给老师："];
            for (int i = 0; i < amount; i ++) {
                [msg appendString:@"[rose]"];
            }
            return msg;
        }else if (model.msg){
            return model.msg;
        }
    }
    return allString;
}

//==================监听ask和chat回来的数据处理======================
- (void)chat:(NSNotification *)notification
{
    NSDictionary * params = notification.userInfo;
    [self.dataSource addObject:params[@"mess"]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chatMessageCome" object:nil];
    
    //记住row的高。、度
//    NSString * msg = params[@"mess"][@"msg"];
//    
//    CGRect rect = [msg boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 20, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
//    CGFloat height = rect.size.height + 30;
//    //        if (self.heightArray.count == 0) {
//    //            height += self.advanceHeight;
//    //        }
//    [self.heightArray addObject:@(height+10)];
    
    [self.tableView reloadData];
//    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.dataSource.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.dataSource.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)recalculateCellHeight;
{
//    [self.heightArray removeAllObjects];
//    int i = 0;
//    for (id obj in self.dataSource) {
//        //抽奖 投票 踢人 广播 禁言
//        NSString * vote_new     = obj[@"vote:new"];
//        NSString * vote_pub     = obj[@"vote:pub"];
//        NSString * lottery_stop = obj[@"lottery:stop"];
//        NSString * broadcast    = obj[@"broadcast"];
//        NSString * chat_disable = obj[@"chat:disable"];
//        NSString * member_kick  = obj[@"member:kick"];
//        
//        if (vote_pub || vote_new || lottery_stop || broadcast || chat_disable || member_kick) {
//            NSString * allString = nil;
//            if (vote_new) {
//                allString = vote_new;
//            }
//            else if (vote_pub)
//            {
//                allString = vote_pub;
//            }
//            else if (lottery_stop)
//            {
//                allString = lottery_stop;
//            }
//            else if (broadcast)
//            {
//                allString = broadcast;
//            }
//            else if (chat_disable)
//            {
//                allString = chat_disable;
//            }
//            else if (member_kick)
//            {
//                allString = member_kick;
//            }
//            CGRect rect = CGRectZero;
//            if (vote_pub || vote_new) {
//                rect = [allString boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 80, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
//            }
//            else
//                rect = [allString boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 40, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
//            
//            if (rect.size.height < 40) {
//                rect.size.height = 40;
//            }
//            [self.heightArray addObject:@(rect.size.height+10)];
//            i ++;
//            continue;
//        }
//        
//        NSString * msg = obj[@"msg"];
//        
//        CGRect rect = [msg boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 20, 9999) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
//        CGFloat height = rect.size.height + 40;
//        [self.heightArray addObject:@(height+10)];
//        
//    }
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TalkfunNewChatCell * cell = [tableView dequeueReusableCellWithIdentifier:@"liveChatCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"TalkfunNewChatCell" owner:nil options:nil][0];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary * params = self.dataSource[indexPath.row];

    [cell configCell:params];
    cell.btnBlock = self.btnBlock;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGFloat height = [self.heightArray[indexPath.row] floatValue];
//    return height;
    NSDictionary * params = self.dataSource[indexPath.row];
    ChatModel *model = [ChatModel mj_objectWithKeyValues:params];
    NSString * string = [self getStringWithModel:model];
    CGFloat width = CGRectGetWidth(self.tableView.frame)-64;
//    if (model.msg || model.amount) {
//        width += 7;
//    }
    NSDictionary * info = [TalkfunUtils assembleAttributeString:string boundingSize:CGSizeMake(width, MAXFLOAT) fontSize:14 shadow:NO];
    NSString * rectStr = info[TextRect];
    CGRect rect = CGRectFromString(rectStr);
//    CGFloat height = rect.size.height+15+16+8 +100  ;
    
     CGFloat height = rect.size.height+35 ;
    return height;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
