//
//  AppDelegate.h
//  TalkfunSDKDemo
//
//  Created by talk－fun on 15/11/24.
//  Copyright © 2015年 talk-fun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,copy) void (^backgroundSessionCompletionHandle)();

+ (void)registerLocalNotification:(NSString *)alertTime AndBody:(NSString *)alerbody;

@end

